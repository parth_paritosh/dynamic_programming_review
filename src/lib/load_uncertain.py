import numpy as np
import pandas as pd
import os
import csv
import cPickle as pickle
import matplotlib.pyplot as plt

with open('simulation_data.pkl', 'rb') as f: 
    plan_horizon, d_x, d_u, d_w, n_collector, beta,\
    EVALUATION_POINTS, INSTANCES, UNCERTAIN_INPUT_FILENAME,\
    WEATHER_FILE, OUTPUT_FILENAME, pump_type = pickle.load(f)

weather_data = os.path.join(os.path.dirname(__file__), 'Data','weather_2017',
                                   WEATHER_FILE+'.csv')
irradiation_data = os.path.join(os.path.dirname(__file__), 'Data',
                            'irradiation_samples', UNCERTAIN_INPUT_FILENAME)

# Value iteration alogrithm class contains only the algorithm and knowledge of problem dimensions
class LoadUncertainParameters(object):
    def __init__(self, d_w = 9, n_w = 10):
        self.d_w = d_w
        self.n_w = n_w

        # Get time based data whenever problem is instantiated
        assert os.path.exists(weather_data)
        with open(weather_data, "rb") as f:
            reader = csv.reader(f)
            list_weather_predictions = list(reader)
        self.list_weather_predictions = list_weather_predictions
        
        assert os.path.exists(irradiation_data)
        I_g = pickle.load( open( irradiation_data, "r" ) )
        self.I_g = I_g
    

    def load_uncertain_val(self, time_step):
        """
        Function reads projected stochastic parameters at time step

        :param time_step: Time step value w.r.t time when value iteration is run
        """
        W = self.list_weather_predictions[time_step]
        W = [float(x) for x in W]
        irr_vals = self.I_g[:,time_step]
        
        W = np.tile(W, (self.n_w, 1))

        W[:, self.d_w - 1] = irr_vals
        return W[:, None]
        

    #### NOT USED FOR ACTUAL SIMULATION; Provides the lowest irradiation value from any sample
    def load_uncertain_instances_lowest_val(self, time_step):
        """
        Function reads projected stochastic parameters at time step

        :param time_step: Time step value w.r.t time when value iteration is run
        """
        W = self.list_weather_predictions[time_step]
        W = [float(x) for x in W]
        irr_vals = self.I_g[:,time_step]
        
        W = np.tile(W, (100, 1))

        W[:, self.d_w - 1] = irr_vals
        W1 = W[:, None]
        irr_min_idx = np.argmin(W1[:, :, -1], axis = 0)
        return W1[irr_min_idx, :, :]
        
    def plot_irradiation_samples(self):
        labelsize = 14
        plt.rcParams['xtick.labelsize'] = labelsize
        plt.rcParams['ytick.labelsize'] = labelsize
        fig = plt.figure()
        ax = fig.add_subplot(111)
        plan_horizon = np.shape(self.I_g)[1]
        line_types = ['-', '--', '-.',':','-', '--', '-.',':','-', '--', '-.',':']
        for idx in range(np.shape(self.I_g)[0]):
            ax.plot(range(plan_horizon), self.I_g[idx, :], line_types[idx%10])
            
        ax.plot(range(plan_horizon), np.min(self.I_g, axis=0), lw=4.)
        ax.set_ylabel(r'Irradiance ($W/m^2$)', fontsize = 18)
        ax.set_xlabel("Time (in hours)", fontsize = 18)
        # ax.legend(range(np.shape(self.I_g)[0]), prop={'size':12})
        ax.set_title("Irradiance samples over 72 hour interval", fontsize=20)
        plt.show()

if __name__ == '__main__':
    print 'LOADING'
    load_uncertain = LoadUncertainParameters( d_w = 9, n_w = 100)
    W1 = load_uncertain.load_uncertain_val( 13 )
    print np.shape(W1),  np.argmin(W1[:, :, -1], axis = 0)
    irr_min_idx = np.argmin(W1[:, :, -1], axis = 0)
    print W1[irr_min_idx, :, :]
    load_uncertain.plot_irradiation_samples() # W1[0:4, :, :],
