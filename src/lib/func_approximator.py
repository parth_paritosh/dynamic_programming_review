r"""

Function Approximators
----------------------

We developed the concept of a ``FunctionApproximator`` so that we can run
the value iteration algorithm with various choices of regression models
in a plug and play fashion.
"""

"""
Remove any use of Theano and ensure that this function works as a standalone from standard libraries
"""

__all__ = ['FunctionApproximator', 'GaussianProcessApproximator',
           'ActiveSubspaceGaussianProcessApproximator', 'ActiveSubspaceGaussianProcessApproximator_',
           'MultioutputApproximator', 'DistributedObject', 'Parallelizer', 'ParallelizedGPRegression', 'distributed_xrange']


from copy import deepcopy
import GPy
from GPy.models import GPRegression
import numpy as np
import functools
from collections import Iterable


def distributed_xrange(n, comm=None):
    """
    If ``comm`` is a MPI communicator, then we distributed the range to
    the processors as evenly as possible.
    """
    if comm is None:
        return xrange(n)
    rank = comm.Get_rank()
    size = comm.Get_size()
    r = n % size
    my_n = n / size
    if rank < r:
        my_n += 1
    my_start = rank * my_n
    if rank >= r:
        my_start += r
    my_end = my_start + my_n
    return xrange(my_start, my_end)



class DistributedObject(object):

    """
    An object that is aware of the parallelization environment.

    :param comm:        An MPI communicator or ``None``.
    :param verbose:     The verbosity level desired. It will be automatically
                        be zero if the rank of the task storing the objecti
                        is not zero.
    """

    @property
    def use_mpi(self):
        """
        Are we using MPI or not?
        """
        return self.comm is not None

    @property
    def verbose(self):
        if self.rank == 0:
            return self._verbose
        else:
            return 0

    def __init__(self, comm=None, verbose=0):
        self.comm = comm
        self._verbose = verbose
        if self.use_mpi:
            self.rank = comm.Get_rank()
            self.size = comm.Get_size()
        else:
            self.rank = 0
            self.size = 1


class Parallelizer(DistributedObject):
    """
    Parallelize the ``optimize_restarts()`` function.
    """

    def __init__(self, **kwargs):
        if not kwargs.has_key('comm'):
            comm = None
        else:
            comm = kwargs['comm']
        if not kwargs.has_key('verbose'):
            verbose = 0
        else:
            verbose = kwargs['verbose']
        super(Parallelizer, self).__init__(comm=comm, verbose=verbose)


    def optimize_restarts(self, num_restarts=10, **kwargs):
        """
        Optimize restarts using MPI.

        :param comm:    The MPI communicator

        When we return, we guarantee that every core has the right model.
        """
        size = self.comm.Get_size()
        rank = self.rank
        comm = self.comm
        my_num_restarts = num_restarts / size
        if my_num_restarts == 0:
            my_num_restarts = 1
        num_restarts = my_num_restarts * size
        if self.verbose >= 2:
            print '> optimizing hyper-parameters using multi-start'
            print '> num available cores:', size
            print '> num restarts:', num_restarts
            print '> num restarts per core:', my_num_restarts
        # Let everybody work with its own data
        self.randomize()
        super(Parallelizer, self).optimize_restarts(num_restarts=my_num_restarts,
                                                    verbose=self.verbose>=2,
                                                    **kwargs)
        if self.use_mpi:
            log_like = np.hstack(comm.allgather(np.array([self.log_likelihood()])))
            max_rank = np.argmax(log_like)
            if self.verbose >= 2:
                print '> maximum likelihood found:', np.max(log_like)
                print '> maximum likelihood found by core:', max_rank
            if rank == max_rank:
                x_opt = self.optimizer_array.copy()
            else:
                x_opt = None
            best_x_opt = comm.bcast(x_opt, root=max_rank)
            if self.verbose >= 2:
                print '> best hyperparameters:', best_x_opt
            self.optimizer_array = best_x_opt


class ParallelizedGPRegression(Parallelizer, GPRegression):
    """
    A parallelized version of GPRegression.
    """

    @property
    def W(self):
        return np.eye(self.input_dim)

    @property
    def theta(self):
        theta = np.hstack([self.kern.variance,
                           self.kern.lengthscale,
                           self.likelihood.variance])
        return theta

    def __init__(self, X, Y, Y_mean=0., Y_std=1., comm=None, verbose=0, **kwargs):
        Parallelizer.__init__(self, comm=comm, verbose=verbose)
        GPRegression.__init__(self, X, Y, **kwargs)
        self.Y_mean = Y_mean
        self.Y_std = Y_std

    def to_array(self):
        """
        Turn the self to an array.
        """
        W = self.W
        theta = self.theta
        X = self.X
        Z = np.dot(X, W)
        Y = np.array(self.Y)
        num_samples = X.shape[0]
        K = self.kern.K(X) + self.likelihood.variance * np.eye(num_samples)
        L = scipy.linalg.cho_factor(K, lower=True)
        b = scipy.linalg.cho_solve(L, Y).flatten()
        Ki = scipy.linalg.cho_solve(L, np.eye(num_samples))
        tmp = []
        tmp.append(W.flatten())
        tmp.append(Z.flatten())
        tmp.append(b.flatten())
        tmp.append(Ki.flatten())
        tmp.append(theta.flatten())
        num_input = self.input_dim
        num_samples = self.num_data
        num_active = W.shape[1]
        return np.hstack([[num_input, num_samples, num_active],
                          np.hstack(tmp), [self.Y_mean, self.Y_std]])

                          
                          

        
class FunctionApproximator(DistributedObject):
    """
    Generic function approximation.

    :param name:       The name of the function approximator.
    :param needs_grad: ``True`` if the function approximator can make use of
                       gradient information.
    """

    def __init__(self, name, needs_grad=False, **kwargs):
        super(FunctionApproximator, self).__init__(**kwargs)
        self.name = name
        self.needs_grad = needs_grad

    def get_other(self):
        """
        Return all parameters of specifying the function.
        """
        raise NotImplementedError('Implement me.')

    def to_theano(self, t_x):
        """
        Turn the approximated function to a theano expression that is a function
        of ``t_x``.
        """
        raise NotImplementedError('Implement me.')

    def to_array(self):
        """
        Return an array representation of the object.
        """
        raise NotImplementedError('Implement me.')

    def fit(self, X, outputs):
        """
        Train the function based on inputs ``X``, outputs.

        ``outputs`` is a list of dictionaries. Each one of these dictionaries,
        contains ``obj`` (a.k.a. the ``y``), ``u`` which is the policy,
        (potentially) ``grad_x_obj`` which is the gradient of ``y`` wrt ``x``,
        and the Lagrange multipliers ``mu_ul``, ``mu_uu``, ``mu_gl``, ``mu_gu``.
        This class is free to use any of this information to build the function.
        """
        raise NotImplementedError('Implement me.')

    def copy(self):
        """
        Provide a way to copy the object.
        """
        return deepcopy(self)

    def get_name(self, outputs, name):
        return np.array([o[name] for o in outputs])

    def get_obj(self, outputs):
        return self.get_name(outputs, 'obj')

    def get_grad_x_obj(self, outputs):
        return self.get_name(outputs, 'grad_x_obj')

    def fit_from_function(self, X, func, dfunc=None):
        """
        Fit the approximator to the function provided in func.
        """
        if dfunc is None:
            outputs = [{'obj': func(x)} for x in X]
        else:
            outputs = [{'obj': func(x),
                        'grad_x_obj': dfunc(x)}
                        for x in X]
        self.fit(X, outputs)

    def get_function(self):
        """
        Return a callable version of the function.
        """
        if self.rank == 0:
            t_x = T.dvector('x')
            t_func, t_other = self.to_theano(t_x)
            func = theano.function([t_x] + t_other, t_func)
        else:
            func = None
        func = self.comm.bcast(func)
        return func


class GaussianProcessApproximator(FunctionApproximator):
    """
    Gaussian process based function approximation.
    """

    def __init__(self, input_dim, k=None, fixed_noise=1e-12, model=None, **kwargs):
        super(GaussianProcessApproximator, self).__init__('Standard GP',
                                                          **kwargs)
        if k is None:
            k = GPy.kern.RBF(input_dim, ARD=True, lengthscale=0.5,
                             variance=1.)
        self.fixed_noise = fixed_noise
        self.input_dim = input_dim
        self.k = k
        self.model = model
        self.is_trained = False

    @property
    def X(self):
        return self.model.X

    @property
    def Y(self):
        return self.model.Y

    def fit(self, X, outputs):
        y = self.get_obj(outputs)
        if y.ndim == 1:
            y = y[:, None]
        assert y.ndim == 2
        self.model = ParallelizedGPRegression(X, y,
                                              comm=self.comm,
                                              verbose=self.verbose,
                                              kernel=self.k.copy())
        if self.fixed_noise is not None:
            self.model.likelihood.variance.unconstrain()
            self.model.likelihood.variance.constrain_fixed(self.fixed_noise)
        self.model.optimize_restarts()
        self.is_trained = True

    def predict(self, X):
        Y_p, V_p = self.model.predict(X)
        return Y_p, V_p
        
    def predict_gradients(self, X):
        grad_p_mean, dv_dX = self.model.predictive_gradients(X)
        # Gives the derivatives on the mean and variance; select the derivatives on mean
        return grad_p_mean
        
    def get_other(self):
        ell = np.array(self.model.kern.lengthscale[:])
        v = float(self.model.kern.variance)
        X = self.model.X
        w = self.model.posterior.woodbury_vector.flatten()
        return ell, v, X, w

    def copy(self):
        if self.model is not None:
            m = self.model.copy()
        else:
            m = None
        return GaussianProcessApproximator(self.input_dim, k=self.k.copy(),
                                           model=m,
                                           verbose=self.verbose,
                                           comm=self.comm)

                                           
    def to_array(self):
        return self.model.to_array()


class ActiveSubspaceGaussianProcessApproximator_(FunctionApproximator):

    """
    A classic active subspace Gaussian process approximator.

    """

    @property
    def W(self):
        return np.array(self.kern.W)

    @property
    def theta(self):
        theta = np.hstack([self.kern.inner_kernel.variance,
                           self.kern.inner_kernel.lengthscale,
                           self.likelihood.variance])
        return theta

    def __init__(self, input_dim, active_dim, fixed_noise=None,
                 k=None, model=None, **kwargs):
        super(ActiveSubspaceGaussianProcessApproximator_, self).__init__('Standard GP',
                                                          needs_grad=True,
                                                          **kwargs)
        if k is None:
            k = GPy.kern.RBF(active_dim, ARD=True, lengthscale=0.5,
                             variance=1.)
        self.input_dim = input_dim
        self.active_dim = active_dim
        self.inner_kernel = k
        self.model = model
        self.fixed_noise = fixed_noise
        self.is_trained = False

    @property
    def X(self):
        return self.model.X

    @property
    def Y(self):
        return self.model.Y

    @property
    def G(self):
        return self.model.G

    def fit(self, X, outputs):
        y = self.get_obj(outputs)
        if y.ndim == 1:
            y = y[:, None]
        assert y.ndim == 2
        G = self.get_grad_x_obj(outputs)
        self.model = ClassicActiveSubspaceGPRegression(X, y, G,
                                              self.inner_kernel.copy(),
                                              comm=self.comm,
                                              verbose=self.verbose)
        if self.fixed_noise is not None:
            self.model.likelihood.variance.unconstrain()
            self.model.likelihood.variance.constrain_fixed(self.fixed_noise)
        self.model.optimize_restarts()
        self.is_trained = True

    def predict(self, X):
        Y_p, V_p = self.model.predict(X)
        return Y_p, V_p
        
    def predict_gradients(self, X):
        grad_p_mean, dv_dX= self.model.predictive_gradients(X)
        # Gives the derivatives on the mean and variance; select the derivatives on mean
        # http://gpy.readthedocs.io/en/deploy/_modules/GPy/core/gp.html
        return grad_p_mean
        
    def get_other(self):
        W = np.array(self.model.kern.W[:])
        ell = np.array(self.model.kern.inner_kernel.lengthscale[:])
        v = float(self.model.kern.inner_kernel.variance)
        X = self.model.X
        Z = np.dot(X, W)
        w = self.model.posterior.woodbury_vector.flatten()
        return W, ell, v, Z, w


    def copy(self):
        if self.model is not None:
            m = self.model.copy()
        else:
            m = None
        if self.inner_kernel is not None:
            k = self.inner_kernel.copy()
        else:
            k = None
        return ActiveSubspaceGaussianProcessApproximator(self.input_dim,
                                                         self.active_dim,
                                                         k=k,
                                                         model=m,
                                                         comm=self.comm,
                                                         verbose=self.verbose)

    def to_array(self):
        return self.model.to_array()

        
class ActiveSubspaceGaussianProcessApproximator(FunctionApproximator):

    """
    A classic active subspace Gaussian process approximator.

    """

    @property
    def W(self):
        return np.array(self.kern.W)

    @property
    def theta(self):
        theta = np.hstack([self.kern.inner_kernel.variance,
                           self.kern.inner_kernel.lengthscale,
                           self.likelihood.variance])
        return theta

    def __init__(self, input_dim, active_dim, fixed_noise=None,
                 k=None, model=None, **kwargs):
        super(ActiveSubspaceGaussianProcessApproximator, self).__init__('Standard GP',
                                                          needs_grad=True,
                                                          **kwargs)
        if k is None:
            k = GPy.kern.RBF(active_dim, ARD=True, lengthscale=0.5,
                             variance=1.)
        self.input_dim = input_dim
        self.active_dim = active_dim
        self.inner_kernel = k
        self.model = model
        self.fixed_noise = fixed_noise
        self.is_trained = False

    @property
    def X(self):
        return self.model.X

    @property
    def Y(self):
        return self.model.Y

    @property
    def G(self):
        return self.model.G

    def fit(self, X, outputs):
        y = self.get_obj(outputs)
        if y.ndim == 1:
            y = y[:, None]
        assert y.ndim == 2
        G = self.get_grad_x_obj(outputs)
        self.model = ClassicActiveSubspaceGPRegression(X, y, G,
                                              self.inner_kernel.copy(),
                                              comm=self.comm,
                                              verbose=self.verbose)
        if self.fixed_noise is not None:
            self.model.likelihood.variance.unconstrain()
            self.model.likelihood.variance.constrain_fixed(self.fixed_noise)
        self.model.optimize_restarts()
        self.is_trained = True

    def get_other(self):
        W = np.array(self.model.kern.W[:])
        ell = np.array(self.model.kern.inner_kernel.lengthscale[:])
        v = float(self.model.kern.inner_kernel.variance)
        X = self.model.X
        Z = np.dot(X, W)
        w = self.model.posterior.woodbury_vector.flatten()
        return W, ell, v, Z, w


    def copy(self):
        if self.model is not None:
            m = self.model.copy()
        else:
            m = None
        if self.inner_kernel is not None:
            k = self.inner_kernel.copy()
        else:
            k = None
        return ActiveSubspaceGaussianProcessApproximator(self.input_dim,
                                                         self.active_dim,
                                                         k=k,
                                                         model=m,
                                                         comm=self.comm,
                                                         verbose=self.verbose)

    def to_array(self):
        return self.model.to_array()


class MultioutputApproximator(FunctionApproximator):
    """
    Multioutput approximator.

    :parma num_out:             The number of outputs.
    :param func_approximator:   Either a single approximator object for
                                each output, or just one approximator
                                for everything.
    """

    def __init__(self, num_out, func_approximator, **kwargs):
        super(MultioutputApproximator, self).__init__('Multioutput', **kwargs)
        assert num_out >= 1
        self.num_out = num_out
        if not isinstance(func_approximator, Iterable):
            func_approximator = [func_approximator.copy()
                                 for _ in range(num_out)]
        assert len(func_approximator) == num_out
        self.func_approximators = func_approximator

    def get_other(self):
        other = []
        for f in self.func_approximators:
            other += f.get_other()
        return other

    def fit(self, X, outputs):
        """
        There is some room for fixing the parallelization here.
        """
        for i in xrange(self.num_out):
            out_i = [{'obj': o['obj'][i]} for o in outputs]
            fap = self.func_approximators[i]
            if fap.needs_grad:
                for n in xrange(len(outputs)):
                    out_i[n]['grad_x_obj'] = outputs[i]['grad_x_obj'][i, :]
            fap.fit(X, out_i)
        self.is_trained = True

    def predict(self, X):
        f_mean = []
        f_variance = [] 
        for f in self.func_approximators:
            f_m, f_var = f.predict(X)
            f_mean.append(f_m)
            f_variance.append(f_var)
        return f_mean, f_variance
        
    def predict_gradients(self, X):
        for f in self.func_approximators:
            f_grad_n = f.predict_gradients(X)
            # Create the concatenation in for loop
        return f_grad_n
    
    def to_theano(self, t_x):
        t_ms = []
        t_others = []
        for f in self.func_approximators:
            t_m, t_other, _ = f.to_theano(t_x, compile=False)
            t_ms.append(t_m)
            t_others += t_other
        t_ms = T.as_tensor(t_ms)
        if self.rank == 0:
            ms = theano.function([t_x] + t_others, t_ms)
        else:
            ms = None
        ms = self.comm.bcast(ms)
        return t_ms, t_others, ms

    def to_array(self):
        adims = []
        data = []
        for m in self.func_approximators:
            tmp = m.to_array()
            adims += [tmp[2]]
            data += [tmp[3:]]
        return np.hstack([tmp[0], self.num_out, tmp[1]] +  adims + data)
