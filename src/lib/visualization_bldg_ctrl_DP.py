"""
DP visualization:
+++++++++++++++++++++++++++++++++
Dynamic programming visualization
+++++++++++++++++++++++++++++++++

It has methods that graph visualizations from data coming out of dynamic programming runs at any instant.

Functions:
Plot optimal inputs as heatmap
Predicted value function at any time step
Plot a grid of value functions at different time steps
Plot involved uncertain samples 
Plot certain weather predictions
Simulate and plot temperature evolution for an uncertain sample
Uncertainty propagation plot for a temperature state

And saves them to paper_images folder
"""

import numpy as np
import math
import matplotlib.pyplot as plt
import GPy
# import design
from scipy.optimize import minimize
# import seaborn as sns
import os
from scipy.ndimage.filters import gaussian_filter
import pickle
from matplotlib.font_manager import FontProperties
from mpi4py import MPI as mpi

from func_approximator import *
from evaluation_points_building_control import DiscretizedSpaceforEvaluation
from stochastic_dynamics_building_control import StochasticDynProgProblem
from load_uncertain import LoadUncertainParameters

comm = mpi.COMM_WORLD
FONTSIZE = 14

from pylab import rcParams

class plotting(DiscretizedSpaceforEvaluation):

    def __init__(self, d_x = 43, plan_horizon = 48):
        # np.random.seed(sum(map(ord, "aesthetics")))
        self.d_x = d_x
        self.plan_horizon = plan_horizon
        super(plotting, self).__init__(self.d_x)

    def predict_plot_VF(self, VF_x_horizon, X):
        """
        :param fap: Gaussian process approximator including value function info

        Plot value function by predicting from GP approximator class
        """
        # Set figure size
        rcParams['figure.figsize'] = 6, 6
        val_func = VF_x_horizon[:, 11]
        
        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')
        ax.scatter(X[:, 1], X[:, 3], val_func, c='r', marker='o')
        ax.set_ylabel('Tank Temperature ($\degree$C)', fontsize=FONTSIZE)
        ax.set_xlabel('Room Temperature ($\degree$C)', fontsize=FONTSIZE)
        ax.set_title('Value function at time = 11', fontsize=FONTSIZE)
        fig.savefig('./paper_images/pred_VF_pts.png', bbox_inches='tight')
        return 1
        
    def predict_plot_VF_contour(self, VF_x_horizon, X):
        """
        :param fap: Gaussian process approximator including value function info

        Plot contour plot of value function by predicting from GP approximator
        """
        # Set figure size
        rcParams['figure.figsize'] = 6, 6
        val_func = VF_x_horizon[:, 11]
        # Number of points for generating function variation along a direction
        n_points = 200
        
        input_dimension = 4
        k = GPy.kern.RBF(input_dimension, ARD=True, lengthscale=1.,
                             variance=1.)
        fap = GaussianProcessApproximator(input_dimension,
                                          k=k,
                                          fixed_noise=None,
                                          comm=comm,
                                          verbose=0)
        outputs = [{'obj': [val_func[idx]]} for idx, x in enumerate(X)]
        fap.fit(X[:, 0:4], outputs)
        
        # Obtain a grid with constant values of X[:, 0] and X[:, 2]
        X_unif = self.get_grid_points_constants(n_points)
        X_grid, Y_grid = np.meshgrid(X_unif[:, 1], X_unif[:, 3])
        X_predict = self.grid_point_samples(X_grid, Y_grid)
        
        VF_m, VF_var = fap.predict(X_predict[:, 0:4])
        VF_samples = np.random.normal(VF_m, VF_var)
        
        VF_samples = np.reshape(VF_samples, (n_points, n_points))
        VF_m = np.reshape(VF_m, (n_points, n_points))
        
        fig = plt.figure()
        ax = fig.add_subplot(111)#, projection='3d')
        #CS = ax.contourf(X_grid, Y_grid, VF_m)
        #CS = ax.pcolormesh(X_grid, Y_grid, VF_m)
        CS = ax.imshow(VF_m, origin="lower")
        fig.colorbar(CS, orientation='vertical')
        # plt.clabel(CS, inline=1, fontsize=10)
        ax.set_ylabel('$T_{tank}$ ($\degree$C)', fontsize=FONTSIZE)
        ax.set_xlabel('$T_{room}$ ($\degree$C)', fontsize=FONTSIZE)
        
        ax.set_title('Value function at time = 11', fontsize=FONTSIZE)
        fig.savefig('./paper_images/pred_VF_pts_contour_20_20'+'.png', bbox_inches='tight')
        return 1
        
    def predict_plot_VF_mesh(self, fap, X):
        """
        :param fap: Gaussian process approximator including value function info

        Surface plot of value function by predicting from GP approximator
        """
        # Set figure size
        rcParams['figure.figsize'] = 6, 6
        
        # X = self.get_design_points(200)
        X = self.get_uniform_grid_points(200)

        VF_m, VF_var = fap.predict(X[:, 0:4])

        X, Y = np.meshgrid(X[:, 1], X[:, 3])
        
        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')
        # ax.scatter(X[:, 1], X[:, 3], VF_m, c='r', marker='o')
        # ax.plot_wireframe(X[:, 1], X[:, 3], VF_m)
        ax.plot_surface(X, Y, VF_m)
        ax.set_ylabel('Tank Temperature ($\degree$C)', fontsize=FONTSIZE)
        ax.set_xlabel('Room Temperature ($\degree$C)', fontsize=FONTSIZE)
        ax.set_title('Predicted value function (at time=11)', fontsize=FONTSIZE)
        fig.savefig('./paper_images/pred_VF_mesh.png', bbox_inches='tight')
        return 1
    
    def grid_point_samples(self, X_grid, Y_grid):
        """
        Obtain grid points in tank and room temperatures by 
        setting other dimensions constant
        """
        Tc_t_dim = 39
        
        # Do sampling for the points in mesh
        X = np.zeros((len(X_grid.flatten()), self.d_x - Tc_t_dim))
        X[:, 1] = X_grid.flatten()
        X[:, 3] = Y_grid.flatten()
        # For T_envelope
        #X[:, 0] = X[:, 1] - np.random.exponential(scale = 1, \
            #size = (len(X[:, 1]), 1)).flatten() - 1
        # T_fl
        #X[:, 2] = X[:, 1] + np.random.exponential(scale = 1, \
            #size = (len(X[:, 1]), 1)).flatten() + 1
        X[:, 0] = 20.*np.ones((len(X[:, 1])))
        X[:, 2] = X[:, 0]
        Tc_t = np.random.rand(Tc_t_dim, 1) + 5
        X = np.hstack((X, np.repeat(Tc_t.T, len(X[:, 1]), axis=0)))
        return X
    
    def predict_plot_policy(self, u_opt_x_horizon, X):
        """
        :param u_opt_x_horizon: Optimal input matrix

        Plot policy function by predicting from GP approximator
        """
        
        # Set figure size
        rcParams['figure.figsize'] = 6, 6
        
        policy_func = u_opt_x_horizon[:, 11]
        
        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')
        ax.scatter(X[:, 1], X[:, 3], 1e2*policy_func, c='r', marker='o')
        ax.set_ylabel('Tank Temperature ($\degree$C)', fontsize=FONTSIZE)
        ax.set_xlabel('Room Temperature ($\degree$C)', fontsize=FONTSIZE)
        ax.set_zlabel('Optimal inputs in $kW$', fontsize=FONTSIZE)
        ax.set_title('Policy function (at time=11)', fontsize=FONTSIZE)
        fig.savefig('./paper_images/pred_policy_pts.png', bbox_inches='tight')
        return 1
    
    def predict_plot_policy_mesh(self, u_opt_x_horizon, X):
        """
        :param u_opt_x_horizon: Optimal input matrix

        Plot policy function as a 3D plot by using predictions from 
        GP approximator over a uniform grid
        """
        
        # Set figure size
        rcParams['figure.figsize'] = 6, 6
        
        policy_func = u_opt_x_horizon[:, 11]

        input_dimension = 4
        k = GPy.kern.RBF(input_dimension, ARD=True, lengthscale=1.,
                             variance=1.)
        fap = GaussianProcessApproximator(input_dimension,
                                          k=k,
                                          fixed_noise=None,
                                          comm=comm,
                                          verbose=0)
        outputs = [{'obj': [policy_func[idx]]} for idx, x in enumerate(X)]
        fap.fit(X[:, 0:4], outputs)
        
        # Number of points for generating function variation along a direction
        n_points = 200
        Tc_t_dim = 39
        # X = self.get_design_points(200)
        X = self.get_uniform_grid_points(n_points)
        
        # Generate a mesh from the obtained equal spaced points
        X_grid, Y_grid = np.meshgrid(X[:, 1], X[:, 3])
        
        X = self.grid_point_samples(X_grid, Y_grid)
        
        VF_m, VF_var = fap.predict(X[:, 0:4])
        VF_m = np.reshape(VF_m, (n_points, n_points))
        # print np.shape(X), np.shape(Y_grid), np.shape(VF_m)
        
        fig = plt.figure()
        ax = fig.add_subplot(111)#, projection='3d')
        # ax.scatter(X[:, 1], X[:, 3], VF_m, c='r', marker='o')
        # ax.plot_wireframe(X[:, 1], X[:, 3], VF_m)
        # ax.plot_surface(X, Y, VF_m)
        CS = ax.contourf(X_grid, Y_grid, 1e2*VF_m)
        fig.colorbar(CS, orientation='vertical')
        ax.set_ylabel('Tank Temperature ($\degree$C)', fontsize=FONTSIZE)
        ax.set_xlabel('Room Temperature ($\degree$C)', fontsize=FONTSIZE)
        ax.set_title('Predicted policy function (at time=11)', fontsize=FONTSIZE)
        fig.savefig('./paper_images/pred_policy_interpolate.png', bbox_inches='tight')
        return 1
        
    def VF_grid(self, VF_x_horizon, X):
        """
        Plotting the evolution of value function over time in a run of dynamic programming
        """
        rcParams['figure.figsize'] = 11, 8
        eval_pts = np.shape(VF_x_horizon)[0]
        plan_horizon = np.shape(VF_x_horizon)[1]

        grid_side = 4
        fig = plt.figure()
        for rows in range(grid_side):
            for cols in range(grid_side+2):
                t_idx = rows*(grid_side+2)+cols
                ax = fig.add_subplot(grid_side, grid_side+2, t_idx + 1, projection='3d')
                ax.scatter(X[:, 1], X[:, 3], VF_x_horizon[:, t_idx], c='r')#, marker='o')
                ax.view_init(elev=20., azim=30.)
                
                start, end = ax.get_xlim()
                stepsize = (end-start)/2.
                tick_values = np.arange(start, end+stepsize, stepsize)
                ax.xaxis.set_ticks(tick_values)
                start, end = ax.get_ylim()
                stepsize = (end-start)/2.
                ax.yaxis.set_ticks(np.arange(start, end+stepsize, stepsize))
                ax.view_init(elev=20., azim=150.)
                
                ax.set_ylabel('Tank')
                ax.set_xlabel('Room')
                ax.set_title(t_idx+1)
        fig.suptitle('Variation in value function in simulation steps', fontsize = 14)
        fig.savefig('./paper_images/VF_grid_pts.png')
        return 1

    def VF_grid_tank(self, VF_x_horizon, X):
        """
        Plot variation of value function w.r.t. tank temperature across time steps
        Predict at each time step
        """
        rcParams['figure.figsize'] = 11, 8
        eval_pts = np.shape(VF_x_horizon)[0]
        plan_horizon = np.shape(VF_x_horizon)[1]

        grid_side = 4
        fig = plt.figure()
        for rows in range(grid_side):
            for cols in range(grid_side+2):
                t_idx = rows*(grid_side+2)+cols
                ax = fig.add_subplot(grid_side, grid_side+2, t_idx + 1)
                ax.scatter(X[:, 3], VF_x_horizon[:, t_idx], c='r', s=0.3)
                ax.set_xlabel('Tank')
                ax.set_title(t_idx+1)
        fig.suptitle('Variation in value function w.r.t. tank temperature in simulation steps', fontsize = 14)
        fig.savefig('./paper_images/VF_grid_tank.png', bbox_inches='tight')
        return 1
        
    def VF_grid_tank_mesh(self, VF_x_horizon, X):
        """
        Plot variation of value function w.r.t. tank temperature across time steps
        Predict at each time step
        """
        rcParams['figure.figsize'] = 11, 8
        eval_pts = np.shape(VF_x_horizon)[0]
        plan_horizon = np.shape(VF_x_horizon)[1]
        X = self.get_uniform_grid_points(200)
        X_, Y = np.meshgrid(X[:, 1], X[:, 3])

        grid_side = 4
        fig = plt.figure()
        fig.subplots_adjust(hspace=.5)
        for rows in range(grid_side+2):
            for cols in range(grid_side):
                t_idx = rows*(grid_side)+cols
                ax = fig.add_subplot(grid_side+2, grid_side, t_idx + 1, projection='3d')
                
                input_dimension = 4
                val_func = VF_x_horizon[:, t_idx]
                fap = GaussianProcessApproximator(input_dimension,
                                                  fixed_noise=None,
                                                  comm=comm,
                                                  verbose=0)
                outputs = [{'obj': [val_func[idx]]} for idx, x in enumerate(X)]
                fap.fit(X[:, 0:4], outputs)
                VF_m, VF_var = fap.predict(X[:, 0:4])
                VF_m_flat = [float("{0:.2f}".format(float(val))) for idx, val in enumerate(VF_m)]
                ax.plot_surface(X_, Y, VF_m_flat)
                ax.view_init(elev=30., azim=30.)
                
                start, end = ax.get_xlim()
                stepsize = (end-start)/2.
                ax.xaxis.set_ticks(np.arange(start, end+stepsize, stepsize))
                start, end = ax.get_ylim()
                stepsize = (end-start)/2.
                ax.yaxis.set_ticks(np.arange(start, end+stepsize, stepsize))
                start, end = ax.get_zlim()
                stepsize = (end-start)/2.
                ax.zaxis.set_ticks(np.arange(start, end+stepsize, stepsize))
                
                ax.set_ylabel('Tank')
                ax.set_xlabel('Room')
                ax.set_title('Time '+str(t_idx+1), color='b')
        fig.suptitle('Variation in value function w.r.t. tank temperature \n in simulation steps', fontsize = 14)
        fig.savefig('./paper_images/VF_grid_tank_mesh.png')
        return 1

    def VF_grid_tank_contour(self, VF_x_horizon, X):
        """
        Plot variation of value function w.r.t. tank temperature across time steps
        Predict at each time step
        """
        rcParams['figure.figsize'] = 11, 8
        eval_pts = np.shape(VF_x_horizon)[0]
        plan_horizon = np.shape(VF_x_horizon)[1]
        n_points = 200
        X_unif = self.get_uniform_grid_points(n_points)
        X_grid, Y_grid = np.meshgrid(X_unif[:, 1], X_unif[:, 3])
        X_predict = self.grid_point_samples(X_grid, Y_grid)

        grid_side = 4
        fig = plt.figure()
        fig.subplots_adjust(hspace=.5)
        for rows in range(grid_side+2):
            for cols in range(grid_side):
                t_idx = rows*(grid_side)+cols
                print t_idx
                ax = fig.add_subplot(grid_side+2, grid_side, t_idx + 1)
                
                input_dimension = 4
                val_func = VF_x_horizon[:, t_idx]
                fap = GaussianProcessApproximator(input_dimension,
                                                  fixed_noise=None,
                                                  comm=comm,
                                                  verbose=0)
                outputs = [{'obj': [val_func[idx]]} for idx, x in enumerate(X)]
                fap.fit(X[:, 0:4], outputs)
                VF_m, VF_var = fap.predict(X_predict[:, 0:4])
                VF_m_flat = [float("{0:.2f}".format(float(val))) for idx, val in enumerate(VF_m)]
                VF_m = np.reshape(VF_m_flat, (n_points, n_points))
                
                # Set contour plot label and 
                CS = ax.contourf(X_grid, Y_grid, VF_m)
                cbar = fig.colorbar(CS, orientation='vertical')
                contour_ticks = CS.levels
                contour_ticks = contour_ticks.tolist()
                for idx, val in enumerate(contour_ticks):
                    if (idx == 1) or (idx == len(contour_ticks)-1):
                        pass
                    else:
                        contour_ticks[idx] = ''
                
                start = np.min(VF_m)
                end = np.max(VF_m)
                stepsize = (end-start)/2.
                cbar.ax.set_yticklabels(contour_ticks)
                
                start, end = ax.get_xlim()
                stepsize = (end-start)/2.
                # ax.xaxis.set_ticks(np.arange(start, end+stepsize, stepsize))
                ax.xaxis.set_ticks([])
                start, end = ax.get_ylim()
                stepsize = (end-start)/2.
                # ax.yaxis.set_ticks(np.arange(start, end+stepsize, stepsize))
                ax.yaxis.set_ticks([])
                
                # ax.set_ylabel('Tank')
                # ax.set_xlabel('Room')
                ax.set_title('Time '+str(t_idx+1), color='b')
        fig.suptitle('Variation in value function w.r.t. tank temperature \n in simulation steps', fontsize = 14)
        fig.text(0.5, 0.04, 'Room temperature ($17-25 \, \degree$C)', ha='center')
        fig.text(0.04, 0.5, 'Tank temperature ($25-55 \, \degree$C)', va='center', rotation='vertical')
        fig.savefig('./paper_images/VF_grid_tank_contour.png')
        return 1
        
    def VF_grid_tank_contour_4(self, VF_x_horizon, X):
        """
        Plot variation of value function w.r.t. tank temperature across time steps
        Predict at each time step
        """
        rcParams['figure.figsize'] = 11, 3
        eval_pts = np.shape(VF_x_horizon)[0]
        plan_horizon = np.shape(VF_x_horizon)[1]
        n_points = 200
        X_unif = self.get_grid_points_constants(n_points)
        X_grid, Y_grid = np.meshgrid(X_unif[:, 1], X_unif[:, 3])
        X_predict = self.grid_point_samples(X_grid, Y_grid)

        grid_col = 4
        time_index = [0, 7, 15, 23]
        fig = plt.figure()
        fig.subplots_adjust(hspace=.5)
        for cols in range(len(time_index)):
            idx_place = cols
            t_idx = time_index[cols]
            print t_idx
            ax = fig.add_subplot(1, grid_col, idx_place + 1)
            
            input_dimension = 4
            val_func = VF_x_horizon[:, t_idx]
            fap = GaussianProcessApproximator(input_dimension,
                                              fixed_noise=None,
                                              comm=comm,
                                              verbose=0)
            outputs = [{'obj': [val_func[idx]]} for idx, x in enumerate(X)]
            fap.fit(X[:, 0:4], outputs)
            #print fap.model, fap.model.rbf.lengthscale
            VF_m, VF_var = fap.predict(X_predict[:, 0:4])
            VF_m_flat = [float("{0:.2f}".format(float(val))) for idx, val in enumerate(VF_m)]
            VF_m = np.reshape(VF_m_flat, (n_points, n_points))
            
            # Hacking the axis limits for uniform visualization
            VF_m[0,0] = -10
            VF_m[0,1] = 80
            
            # Set contour plot label and 
            CS = ax.contourf(X_grid, Y_grid, VF_m)
            cbar = fig.colorbar(CS, orientation='vertical')
            contour_ticks = CS.levels
            contour_ticks = contour_ticks.tolist()
            for idx, val in enumerate(contour_ticks):
                if (idx == 1) or (idx == len(contour_ticks)-1):
                    pass
                else:
                    contour_ticks[idx] = ''
            
            start = np.min(VF_m)
            end = np.max(VF_m)
            stepsize = (end-start)/2.
            cbar.ax.set_yticklabels(contour_ticks)
            
            start, end = ax.get_xlim()
            stepsize = (end-start)/2.
            ax.xaxis.set_ticks(np.arange(start, end+1, stepsize))
            start, end = ax.get_ylim()
            stepsize = (end-start)/2.
            #ax.yaxis.get_ticks()
            
            ax.set_ylabel('$T_{tank}$ ($\degree$C)')
            ax.set_xlabel('$T_{room}$ ($\degree$C)')
            ax.set_title('Time '+str(t_idx+1), color='b')
        #fig.suptitle('Variation in value function w.r.t. tank temperature in simulation steps', fontsize = 14)
        fig.tight_layout()
        # fig.text(0.48, 0.05, '$T_{room}$ ($\degree$C)', ha='center')
        # fig.text(0.0, 0.5, '$T_{tank}$ ($\degree$C)', va='center', rotation='vertical')
        fig.savefig('./paper_images/VF_grid_tank_contour_4_20_20.png')
        return 1
        
    def simulate_trajectory(self, bldg_control,
                            load_uncertain_params, X, u_opt_x_horizon):
        """
        Simulate system trajectory with optimal policy on first sample
        of uncertain inputs
        """
        eval_pts = X.shape[0]
        d_x_ = X.shape[1]
        plan_horizon = u_opt_x_horizon.shape[1]
        current_X = X
        X_output_predicted = np.zeros((plan_horizon, eval_pts, d_x_))

        for time_step in range(plan_horizon):
            W = load_uncertain_params.load_uncertain_val(time_step)
            print "Uncertain inputs are", W
            # Train Input values against original X
            U_for_training = u_opt_x_horizon[:, plan_horizon - time_step - 1]
            input_dimension = 4
            u_GP_approx = GaussianProcessApproximator(input_dimension,
                                                    fixed_noise = None,
                                                    comm=comm,
                                                    verbose=0)
            # Check if the fit works here
            outputs = [{'obj': [U_for_training[idx]]} for idx, x in enumerate(X)]
            u_GP_approx.fit(X[:, 0:4], outputs)
            u_pred_mean, u_pred_var = u_GP_approx.predict(current_X[:, 0:4])
            print "X values and predicted inputs are", current_X[:, 0:4], u_pred_mean
            for x_idx, curr_x in enumerate(current_X):
                X_output_predicted[time_step, x_idx, :] = bldg_control.\
                    f(np.reshape(curr_x, (-1, 1)), u_pred_mean[x_idx],\
                     W[0, 0]).flatten()
            current_X = X_output_predicted[time_step]

        return X_output_predicted[:,:,0:4]

    def simulate_temperatures(self, bldg_control,
                            load_uncertain_params, X, u_opt_x_horizon):
        """
        Simulate evolution of temperatures given the policy function derived from optimal inputs data

        :param bldg_control: The class including dynamic programming details
        :param X: Design points over which simulation steps are implemented
        :param u_opt_x_horizon: Optimal inputs at each time step of X

        Returns array of simulated temperatures
        """
        eval_pts = X.shape[0]
        d_x_ = X.shape[1]
        plan_horizon = u_opt_x_horizon.shape[1]
        current_X = X
        X_output_predicted = np.zeros((plan_horizon, eval_pts, d_x_))

        for time_step in range(plan_horizon):
            W = load_uncertain_params.load_uncertain_val(time_step)
            # Train Input values against original X
            U_for_training = u_opt_x_horizon[:, plan_horizon - time_step - 1]
            input_dimension = 4
            u_GP_approx = GaussianProcessApproximator(input_dimension,
                                                    fixed_noise = None,
                                                    comm=comm,
                                                    verbose=0)
            # Check if the fit works here
            outputs = [{'obj': [U_for_training[idx]]} for idx, x in enumerate(X)]
            u_GP_approx.fit(X[:, 0:4], outputs)
            u_pred_mean, u_pred_var = u_GP_approx.predict(current_X[:, 0:4])
            # print "X values and predicted inputs are", current_X[:, 0:4], u_pred_mean
            for x_idx, curr_x in enumerate(current_X):
                X_output_predicted[time_step, x_idx, :] = bldg_control.\
                    f(np.reshape(curr_x, (-1, 1)), u_pred_mean[x_idx],\
                     W[0, 0]).flatten()
            current_X = X_output_predicted[time_step]

        return X_output_predicted[:,:,0:4]

    def plot_heatmap(self, u_opt_x_horizon):
        """
        Plot optimal policy as a heatmap
        """
        rcParams['figure.figsize'] = 11, 8
        column_labels = list(range(1, 50, 5))
        row_labels = list(range(1,100, 10))
        u_opt_x_horizon=1e2*u_opt_x_horizon
        fig, ax = plt.subplots()
        heatmap = ax.pcolor(u_opt_x_horizon, cmap='inferno', \
                vmin=np.min(u_opt_x_horizon), vmax=np.max(u_opt_x_horizon))#,\
                #interpolation='nearest', origin='lower')

        # put the major ticks at the middle of each cell, notice "reverse" use of dimension
        # ax.set_yticks(np.arange(u_opt_x_horizon.shape[0])+0.5, minor=False)
        # ax.set_xticks(np.arange(u_opt_x_horizon.shape[1])+0.5, minor=False)

        ax.set_xticklabels(row_labels, minor=True)
        ax.set_yticklabels(column_labels, minor=True)
        # plt.colorbar()
        fig.colorbar(heatmap, orientation='vertical')
        ax.set_xlabel('Time', fontsize=FONTSIZE)
        ax.set_ylabel('Initial points', fontsize=FONTSIZE)
        ax.set_title('Optimal input (in $kW$) w.r.t. Time', fontsize=FONTSIZE+2)
        fig.savefig('./paper_images/'+'heatmap.png', bbox_inches='tight')
        fig.savefig('./paper_images/'+'heatmap.pdf', bbox_inches='tight')
        return ax
        
    def threeD_to_animation(self, u_val, time_step):
        cwd = os.getcwd()
        cwd = cwd.replace('\\', '/')
        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')
        ax.scatter(X[:, 1], X[:, 3], np.array(u_val), c='r', marker='o')
        filename_str = cwd +'/Images/'+ 'u_optimal_at_time_step = ' \
                        +str(time_step)+'.png'
        fig.savefig(filename_str, bbox_inches='tight')
        for ii in xrange(0,360,10):
            ax.view_init(elev=10.+10.*np.abs(np.sin(ii)), azim=ii)
            fig.savefig(cwd +'/Images/'+ 'u_optimal_at_time_step = ' \
                +str(time_step)+"movie%d.png" % ii)
        return fig
        
    def plot_time_dependent(self, load_uncertain_params,
                                plan_horizon):
        """
        Plot time dependent deterministic parameters over the 
        planning horizon
        """
        rcParams['figure.figsize'] = 8, 6
        T_out = np.zeros(plan_horizon)
        # ghi_clr = np.zeros(plan_horizon)
        RH = np.zeros(plan_horizon)
        for time_step in range(plan_horizon):
            T_out[time_step] = load_uncertain_params.load_uncertain_val(time_step)\
                                [0,0][0]
            # ghi_clr[time_step] = load_uncertain_params.load_uncertain_val(time_step)\
                                # [0,0][6]
            RH[time_step] = load_uncertain_params.load_uncertain_val(time_step)[0,0][7]
        labelsize = 12
        plt.rcParams['xtick.labelsize'] = labelsize
        plt.rcParams['ytick.labelsize'] = labelsize
        fig = plt.figure()
        ax = fig.add_subplot(211)
        ax.plot(range(plan_horizon), T_out)#, label = 'Outside Temperature ($\degree$C)')
        ax.set_ylabel('Outside Temperature ($\degree$C)')
        # ax.plot(range(plan_horizon), ghi_clr, label = 'Clear sky horizontal irradiance')
        ax1 = fig.add_subplot(212)
        ax1.plot(range(plan_horizon), RH)#, label = 'Relative humidity (%)')
        ax1.set_ylabel('Relative humidity (%)')
        ax1.set_xlabel("Time (in hours)", fontsize = 14)
        # ax.set_ylim([0.,120.])
        fig.suptitle('Time dependent parameters', fontsize = 14)
        fontP = FontProperties()
        # fontP.set_size('small')
        # plt.legend(loc='upper right', ncol=1, fancybox=True, shadow=True, prop = fontP)
        fig.savefig('./paper_images/time_dependent.png', bbox_inches='tight')
        return ax
        
    def simulate_trajectory_plots(self, bldg_control,
                                load_uncertain_params, X, u_opt_x_horizon):
        rcParams['figure.figsize'] = 11, 8
        eval_pts = X.shape[0]
        plan_horizon = u_opt_x_horizon.shape[1]

        simulated_states = self.simulate_temperatures(bldg_control, load_uncertain_params,\
             X, u_opt_x_horizon)
        fig = plt.figure()
        ax = fig.add_subplot(211)
        # xposition = [5, 19, 29, 43]
        xposition = [5, 19]
        for xc in xposition:
            plt.axvline(x=xc, color='k', linestyle='--')
        for x_idx in range(eval_pts):
            ax.plot(range(plan_horizon), simulated_states[:, x_idx, 1], c='b')
        ax.set_ylabel('Room Temperature ($\degree$C)', fontsize = 14)
        ax = fig.add_subplot(212)
        # xposition = [5, 19, 29, 43]
        xposition = [5, 19]
        for xc in xposition:
            plt.axvline(x=xc, color='k', linestyle='--')
        for x_idx in range(eval_pts):
            ax.plot(range(plan_horizon), simulated_states[:, x_idx, 3], c='b')
        ax.set_ylabel('Tank Temperature ($\degree$C)', fontsize = 14)
        ax.set_xlabel('Time (in hours)', fontsize = 14)

        fig.suptitle('Temperature evolution in building with optimal inputs \n for first uncertain sample', fontsize = 16)
        fig.savefig('./paper_images/simulate_temperatures.png', bbox_inches='tight')
        return 0

    def plot_irradiation_samples(self, load_uncertain_params):
        """
        Plot irradiation samples 
        """
        rcParams['figure.figsize'] = 6, 6
        I_g = load_uncertain_params.return_irradiation_samples()
        labelsize = 14
        plt.rcParams['xtick.labelsize'] = labelsize
        plt.rcParams['ytick.labelsize'] = labelsize
        fig = plt.figure()
        ax = fig.add_subplot(111)
        plan_horizon = np.shape(I_g)[1]
        line_types = ['-', '--', '-.',':','-', '--', '-.',':','-', '--', '-.',':']
        for idx in range(np.shape(I_g)[0]):
            # ax.plot(range(plan_horizon), I_g[idx, :], line_types[idx])
            ax.plot(range(plan_horizon), I_g[idx, :], c = 'b')
        ax.set_ylabel(r'Irradiance ($W/m^2$)', fontsize = FONTSIZE)
        ax.set_xlabel("Time (in hours)", fontsize = FONTSIZE)
        # ax.legend(range(np.shape(I_g)[0]), prop={'size':12})
        ax.set_title("Irradiance samples over 24 hour interval", fontsize=FONTSIZE+2)
        fig.savefig('./paper_images/irr_samples.png', bbox_inches='tight')
        return 1
        
    def scatter_design_points(self, X = None):
        """
        Input: Points in state space
        Output: Scatter plot of design points in four dimensions

        """
        rcParams['figure.figsize'] = 11, 8
        fig = plt.figure()
        ax = fig.add_subplot(311)
        ax.scatter(X[:, 1], X[:, 0], c='r', marker='o')
        ax.plot([min(X[:, 0:1]), max(X[:, 0:1])], [min(X[:, 0:1]), \
            max(X[:, 0:1])], ls="--", c=".3")
        ax.set_ylabel('Envelope temperature ($\degree$C)')
        ax1 = fig.add_subplot(312)
        ax1.scatter(X[:, 1], X[:, 2], c='r', marker='o')
        ax1.plot([min(X[:, 1:2]), max(X[:, 1:2])], [min(X[:, 1:2]), \
            max(X[:, 1:2])], ls="--", c=".3")
        ax1.set_ylabel('Floor temperature($\degree$C)')
        ax2 = fig.add_subplot(313)
        ax2.scatter(X[:, 1], X[:, 3], c='b', marker='o')
        ax2.set_ylabel('Tank temperature($\degree$C)')
        ax2.set_xlabel('Room temperature($\degree$C)')
        fig.savefig('./paper_images/design_pts.png', bbox_inches='tight')
        return 0
        

def simulate_step(bldg_control, load_uncertain_params, 
                    X, u_opt_x_horizon, current_time):
    eval_pts = X.shape[0]
    d_x_ = X.shape[1]
    plan_horizon = u_opt_x_horizon.shape[1]
    current_X = X
    X_output_predicted = np.zeros((eval_pts, d_x_))
    
    W = load_uncertain_params.load_uncertain_val(current_time)
    for x_idx, curr_x in enumerate(current_X):
        X_output_predicted[x_idx, :] = bldg_control.\
            f(np.reshape(curr_x, (-1, 1)), 1e5*u_opt_x_horizon[x_idx, 0],\
             W[0, 0]).flatten()
    
    return X_output_predicted

if __name__ == '__main__':
    print 'VISUALIZATIONS'

    with open('./../simulation_data.pkl', 'rb') as f: 
        plan_horizon, d_x, d_u, d_w, n_collector, beta,\
        EVALUATION_POINTS, INSTANCES, UNCERTAIN_INPUT_FILENAME,\
        WEATHER_FILE, OUTPUT_FILENAME, pump_type = pickle.load(f)
        
        bldg_control = StochasticDynProgProblem(d_x=d_x, d_u=d_u, d_w=d_w, n_collector=n_collector, beta=beta, n_w=INSTANCES)
        load_uncertain_params = LoadUncertainParameters(d_w=d_w, n_w=INSTANCES)

        file_path = "./../"+OUTPUT_FILENAME+".p"
        with open(file_path) as f:
            result_dict = pickle.load(f)
        VF_x_horizon = result_dict['VF_x_horizon']
        u_opt_x_horizon = result_dict['u_opt_x_horizon']
        X = result_dict['X']

        print np.shape(X), np.shape(u_opt_x_horizon), np.shape(VF_x_horizon)
        # print "X values are", X[:, 0:4]
        # print VF_x_horizon[:, 11], u_opt_x_horizon[:, 11]
        val_func = VF_x_horizon[:, 11]

        input_dimension = 4
        fap = GaussianProcessApproximator(input_dimension,
                                          fixed_noise=None,
                                          comm=comm,
                                          verbose=0)
        outputs = [{'obj': [val_func[idx]]} for idx, x in enumerate(X)]
        fap.fit(X[:, 0:4], outputs)
        
        pred_plot = plotting(d_x = 43, plan_horizon = np.shape(VF_x_horizon)[1])   

        # with open("irr_10_48.pcl", 'rb') as f:    
        #     I_g = pickle.load(f) 
        # pred_plot.plot_irradiation_samples(I_g)
        # plt.show()

        # pred_plot.scatter_design_points(X)

        # pred_plot.plot_time_dependent(load_uncertain_params, plan_horizon)
        # pred_plot.plot_irradiation_samples(load_uncertain_params)
        # pred_plot.plot_heatmap(u_opt_x_horizon)

        # 3D scatter plot to view the cluster of x values that yield results
        # Assumption (confirmed via heatmaps): same x values yield positive solutions

        # fig = plt.figure()
        # ax = fig.add_subplot(111, projection='3d')
        # ax.scatter(X[:, 1], X[:, 3], u_opt_x_horizon[:,1], c = 'r', marker='o')

        # Plot optimal input function
        # pred_plot.predict_plot_policy(u_opt_x_horizon, X)
        # pred_plot.predict_plot_policy_mesh(u_opt_x_horizon, X)
        # pred_plot.predict_plot_VF(VF_x_horizon, X)
        pred_plot.predict_plot_VF_contour(VF_x_horizon, X)
        
        # pred_plot.VF_grid(VF_x_horizon, X)
        
        # pred_plot.VF_grid_tank(VF_x_horizon, X)
        
        # pred_plot.VF_grid_tank_mesh(VF_x_horizon, X)
        # pred_plot.VF_grid_tank_contour(VF_x_horizon, X)
        
        pred_plot.VF_grid_tank_contour_4(VF_x_horizon, X)
        
        # pred_plot.simulate_trajectory_plots(bldg_control, load_uncertain_params, X, u_opt_x_horizon)
        plt.show()