import numpy as np
import math
import matplotlib.pyplot as plt
import GPy
import design
import cPickle as pickle

# Class can be extended to create varying discretization w.r.t. time steps        
class DiscretizedSpaceforEvaluation(object):

    def __init__(self, d_x=43):
        self.d_x = d_x
    
    """
    x_t[0]: theoretically there's no bound, but v_t[0] to 30 would be reasonable
    x_t[1]: when t=8-10, take any value from 17-25; when t=10.5-18.5, take any value from 21-25; when t=10.5-18.5, take any value from 15-25; 
    x_t[2]: 19-29
    x_t[3]: 25-55
        
    Since the first three temperatures have high correlations with each other, LHS + normal jitter has been used 
    T_room is the base value, T_floor is higher and T_envelope is lower 
    np.random.exponenetial stays positive and has decent values
    """
    def get_design_points(self, eval_pts=100):
        d_x = 43
        Tc_t_dim = 39
        X = design.latin_center(eval_pts, d_x-Tc_t_dim-2)
        # Make X as 4-dimensional, ensure that index 2 and 4 are different
        X = np.hstack((X, X))
        # T_room
        min_ = 17 
        max_ = 25
        X[:, 1] = min_ + X[:, 1]*(max_ - min_)
        # T_tank
        min_ = 25 
        max_ = 55
        X[:, 3] = X[:, 0]
        X[:, 3] = min_ + X[:, 3]*(max_ - min_)
        # For T_envelope
        X[:, 0] = X[:, 1] - np.random.exponential(scale = 1, \
            size = (eval_pts, 1)).flatten() - 1
        # T_fl
        X[:, 2] = X[:, 1] + np.random.exponential(scale = 1, \
            size = (eval_pts, 1)).flatten() + 1
        
        Tc_t = np.random.rand(Tc_t_dim, 1) + 5
        
        X = np.hstack((X, np.repeat(Tc_t.T, eval_pts, axis=0)))
        return X
    
    def get_uniform_grid_points(self, eval_pts=100):
        """
        Generate uniform grid along dimensions 1 and 3
        """
        d_x = 43
        Tc_t_dim = 39
        X = design.latin_center(eval_pts, d_x-Tc_t_dim)
        
        # T_room
        min_ = 17 
        max_ = 25
        X[:, 1] = np.linspace(min_, max_, eval_pts)
        # T_tank
        min_ = 25 
        max_ = 55
        X[:, 3] = np.linspace(min_, max_, eval_pts)
        # For T_envelope
        X[:, 0] = X[:, 1] - np.random.exponential(scale = 1, \
            size = (eval_pts, 1)).flatten() - 1
        # T_fl
        X[:, 2] = X[:, 1] + np.random.exponential(scale = 1, \
            size = (eval_pts, 1)).flatten() + 1
        
        Tc_t = np.random.rand(Tc_t_dim, 1) + 5
        
        X = np.hstack((X, np.repeat(Tc_t.T, eval_pts, axis=0)))
        return X
        
    def get_grid_points_constants(self, eval_pts=100):
        """
        Generate uniform grid along dimensions 1 and 3 and keep others constant
        """
        d_x = 43
        Tc_t_dim = 39
        X = design.latin_center(eval_pts, d_x-Tc_t_dim)
        
        # T_room
        min_ = 17 
        max_ = 25
        X[:, 1] = np.linspace(min_, max_, eval_pts)
        # T_tank
        min_ = 25 
        max_ = 55
        X[:, 3] = np.linspace(min_, max_, eval_pts)
        # For T_envelope
        X[:, 0] = X[0, 1] - np.random.exponential(scale = 1, \
            size = (1, 1))*np.ones((eval_pts,1)).flatten() - 1
        # T_fl
        X[:, 2] = X[0, 1] + np.random.exponential(scale = 1, \
            size = (1, 1))*np.ones((eval_pts,1)).flatten() + 1
        
        Tc_t = np.random.rand(Tc_t_dim, 1) + 5
        
        X = np.hstack((X, np.repeat(Tc_t.T, eval_pts, axis=0)))
        return X
        
    def scatter_design_points(self, x_t = None):
        """
        Input: Points in state space
        Output: Scatter plot of design points in four dimensions

        """
        X = x_t
        if (x_t == None):
            X = self.get_design_points()
        
        fig = plt.figure()
        ax = fig.add_subplot(311)
        ax.scatter(X[:, 1], X[:, 0], c='r', marker='o')
        ax.plot([min(X[:, 0:1]), max(X[:, 0:1])], [min(X[:, 0:1]), \
            max(X[:, 0:1])], ls="--", c=".3")
        ax.set_ylabel('T_envelope')
        ax1 = fig.add_subplot(312)
        ax1.scatter(X[:, 1], X[:, 2], c='r', marker='o')
        ax1.plot([min(X[:, 1:2]), max(X[:, 1:2])], [min(X[:, 1:2]), \
            max(X[:, 1:2])], ls="--", c=".3")
        ax1.set_ylabel('T_floor')
        ax2 = fig.add_subplot(313)
        ax2.scatter(X[:, 1], X[:, 3], c='b', marker='o')
        ax2.set_ylabel('T_Tank')
        ax2.set_xlabel('T_room')
        plt.show()
        return 0
        
if __name__ == '__main__':
    print 'RUNNING'
    design_points = DiscretizedSpaceforEvaluation()

    des_pts = design_points.get_design_points()
    design_points.scatter_design_points(des_pts)
    pickle.dump(des_pts, open("DP_state_space.p", "wb"))