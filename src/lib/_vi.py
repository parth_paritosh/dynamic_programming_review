r"""
The Value Iteration Algorithm
-----------------------------

The value-iteration algorithm can solve both the infinite and the finite
horizon DP problems.

For an **infinite horizon DP**, the algorithm starts from an arbitrary value
function, e.g., :math:`V_0(\mathbf{x}) = 0` and iterates until convergence the
following expression:

    .. math::

        V_{i+1}(\mathbf{x}) = \max_{\mathbf{u}\in\mathcal{U}(\mathbf{x})}\left[
        r(\mathbf{x}, \mathbf{u}) + \beta V_i(\mathbf{f}(\mathbf{x}, \mathbf{u}))
        \right].

The optimal value function is given by:

    .. math::

        V = \lim_{i\rightarrow\infty} V_i,

under some distance in a function space. Then, we may obtain the optimal,
necessarily stationary, policy :math:`\mu:\mathbb{R}^{d_x}\rightarrow\mathbb{R}^{d_u}`,
by solving the following problem:

    .. math::

        \boldsymbol{\mu}(\mathbf{x}) = \arg\max_{\mathbf{u}\in\mathcal{U}(\mathbf{x})}
            \left[r(\mathbf{x}, u) + \beta V(\mathbf{f}(\mathbf{x},\mathbf{u}))\right].

See :ref:`dihdb_example`.

For a **finite horizon DP**, the algorithm starts from the terminal value
function :math:`V_T(\mathbf{x})` and iterates back to zero by:

    .. math::

        V_{i}(\mathbf{x}) = \max_{\mathbf{u}\in\mathcal{U}(\mathbf{x})}
        \left[r(\mathbf{x}, \mathbf{u}) + \beta V_{i+1}(\mathbf{f}(\mathbf{x},\mathbf{u})\right].

Once the optimal value function for each step is known, then the optimal
policies can be found by:

    .. math::

        \boldsymbol{\mu}_i(\mathbf{x}) = \arg\max_{\mathbf{u}\in\mathcal{U}(\mathbf{x})}
        \left[r(\mathbf{x}, \mathbf{u}) + \beta V_i(\mathbf{f}(\mathbf{x},\mathbf{u})\right].

"""


__all__ = ['ValueIterationAlgorithm']


from . import DistributedObject
from . import distributed_xrange
from . import InfiniteHorizonDPP
from . import FiniteHorizonDPP
from . import FunctionApproximator
from . import GaussianProcessApproximator
from . import GGaussianProcessApproximator
import theano
import numpy as np
import itertools
from tqdm import tqdm

#SS for to create output
import cPickle as pickle

class ValueIterationAlgorithm(DistributedObject):

    """
    Implements the value iteration algorithm.

    :param dpp:             The stochastic dynamic programming problem.
    :param value_func_approximator:  The value function approximator.
    :param X_design:                 The design points. For the time being,
                                     this must be a multiple of the number of
                                     tasks.
    :param use_jac:                  Use the jacobian to evaluate the right
                                     hand side of Bellman's equation.
    :param max_it:                   The maximum number of iterations.
    :param tol:                      The tolerance.
    :param verbose:                  Integer, how much output to print.
                                     0 is nothing.
                                     1 and above prints something.
    """


    @property
    def is_finite(self):
        return isinstance(self.dpp, FiniteHorizonDPP)

    def __init__(self, dpp, X_design,
                 value_func_approximator=None,
                 use_jac=False,
                 initialize_value_func=True,
                 max_it=1000, tol=1e-6, restart_it =1,
                 verbose=0,
                 comm=None):
        super(ValueIterationAlgorithm, self).__init__(comm=comm,
                                                      verbose=verbose)
        d_x = X_design.shape[1]
        if value_func_approximator is None:
            if not use_jac:
                value_func_approximator = GaussianProcessApproximator(d_x,
                                                                      comm=comm)
            else:
                value_func_approximator = GGaussianProcessApproximator()
        if value_func_approximator.needs_grad:
            use_jac = True
        self.dpp = dpp
        t_x = dpp.t_x
        if initialize_value_func:
            if self.is_finite:
                if self.verbose >= 1:
                    print '> finite horizon DPP.'
                    print '> I will just start from the term function.', restart_it
                t_V0 = self.dpp.t_V_term
                t_other0 = []
                other0 = []
            elif isinstance(dpp, InfiniteHorizonDPP):
                if self.verbose >= 1:
                    print '> infinite horizon DPP.'
                    print '> I will just start from a zero term function.'
                t_V0 = 0.
                t_other = []
                other0 = []
            else:
                raise NotImplementedError('I do not know how to initialize a '
                                          + str(type) + '.')
            update_coptp = True
        else:
            t_V0, t_other0 = value_func_approximator.to_theano(t_x)
            other0 = value_func_approximator.get_other()
            update_coptp = False
        self.t_V = [t_V0]
        self.t_other = [t_other0]
        self.other = [other0]
        self.update_coptp = update_coptp
        if self.rank == 0:
            V0 = theano.function([t_x] + t_other0, t_V0,
                                 on_unused_input='ignore')
        else:
            V0 = None
        V0 = self.comm.bcast(V0)
        self.V = [V0]
        self.X_design = X_design
        self.value_func_approximator = value_func_approximator
        self.use_jac = use_jac
        self.max_it = max_it
        self.tol = tol
        self.restart_it = restart_it
        self.dpp.set_up_coptp(self.t_V[0], self.t_other[0], self.other[0])

    def step(self, step_number):
        """
        Do a single step of the value iteration algorithm.
        """
        rank = self.rank
        size = self.size
        comm = self.comm
        X = self.X_design
        dpp = self.dpp
        t_x = dpp.t_x
        coptp = self.dpp.coptp
        vfa = self.value_func_approximator

        if step_number > 1:
            if rank == 0:
                with open('VF_%d.pcl' % (step_number - 1), 'rb') as fd:
                    step_data_dict = pickle.load(fd)
                keys = step_data_dict.keys()
            else:
                step_data_dict = None
            step_data_dict = comm.bcast(step_data_dict)
            self.update_coptp = False
            t_V = step_data_dict['t_V']         # Symbolic value function
                                                # Not needed below
            V = step_data_dict['V']             # Compiled value function
                                                # Not needed below
            t_other = step_data_dict['t_other'] # Symbolic value parameters
                                                # Not needed below
            other = step_data_dict['other']     # Actual value parameters
                                                # Needed below
            dpp.coptp = step_data_dict['coptp'] # Copiled optimization problem
                                                # Needed below
        else:
            other = self.other[-1]

        nd = X.shape[0]
        d = X.shape[1]
        my_jobs = distributed_xrange(nd, comm)
        # The code below calls ``dpp.coppt`` (which is already updated)
        # and requires just ``other``
        my_outputs = [dpp.bellman_operator(X[i,:], other,
                                           get_grad=self.use_jac)
                      for i in tqdm(my_jobs, disable=self.verbose < 1,
                                    unit=' design pts per task', ncols=80)]
        outputs = list(itertools.chain(*comm.allgather(my_outputs)))
        # Perhaps we would like to save all outputs in an HDF5 file.
        # For now:
        self.last_step_data = outputs
        vfa.fit(self.X_design, outputs)
        other = vfa.get_other()
        t_V, t_other, V = vfa.to_theano(t_x)
        other = vfa.get_other()
        # Compile the new problem if this is not the first iteration
        # NOTE: If you want to change the ``vfa`` on the fly this would have
        # to change to ``True`` somehow. Let's leave it for later.
        if self.update_coptp:
            dpp.set_up_coptp(t_V, t_other, other)
            self.update_coptp = False

        # The root saves everything we neeed to restart (and more)
        step_data_dict = {}
        step_data_dict['t_V'] = t_V
        step_data_dict['t_other'] = t_other
        step_data_dict['V'] = V
        step_data_dict['other'] = other
        step_data_dict['coptp'] = dpp.coptp
        if rank == 0:
            with open('VF_%d.pcl' % step_number, 'wb') as fd:
                pickle.dump(step_data_dict, fd,
                            protocol=pickle.HIGHEST_PROTOCOL)

    def solve(self):
        """
        Solve the problem.

        Note that instead of using integration to come up with the error norm,
        we just compare the function values on the design points.

        This will comparison will not work if the design points change from
        iteration to iteration.
        """
        # The first step is special because we may be working with a
        # value function that is analytically defined instead of
        # approximate
        for i in xrange(self.max_it):
            self.step()
            #Y0 = self.value_func_approximator.Y.copy()
            #DeltaV = np.linalg.norm(Y0 - self.value_func_approximator.Y) / \
            #         np.linalg.norm(Y0)
            #if self.verbose >= 1:
            #    print 'it = {0:5d}, DeltaV = {1:1.6e}'.format(i+1, DeltaV)
            #if DeltaV <= self.tol:
            #    if self.verbose >= 1:
            #        print '*** converged ***'
            #   break
        #self.policy_func_approximator.fit(self.X_design, self.last_u)
