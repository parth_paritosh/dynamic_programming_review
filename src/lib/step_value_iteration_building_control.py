import numpy as np
import math
import matplotlib.pyplot as plt
import GPy
from scipy.optimize import minimize
from scipy.optimize import check_grad, approx_fprime
import os
from matplotlib.font_manager import FontProperties
import time
import cPickle as pickle
from mpi4py import MPI as mpi

from func_approximator import *
comm = mpi.COMM_WORLD  

from stochastic_dynamics_building_control import StochasticDynProgProblem

class ValueIterationStep(object):
    def __init__(self, d_x=43, d_u=1, eval_pts=100):
        self.d_x = d_x
        self.d_u = d_u
        self.eval_pts = eval_pts

    def check_violations(self, x0, sdyn_problem, fap, W_time_step):
        """
        :param x0: A point in state space domain
        :param sdyn_problem: Stochastic dynamic problem class
        :param fap: GP based function approximator for value function
        :param W_time_step: Samples of uncertain parameters at time step

        Check whether constraints are satisfied at u = 0k/ 35k
        0 for violated constraints
        """        
        constraint_flag = [1] * 2  
        constraint_values_u_x = np.zeros((2, 7))

        max_permissible_input = sdyn_problem.least_max_input(x0, W_time_step)
        urange = 1e-2 * np.array([0., max_permissible_input])

        for u_idx, u_val in enumerate(urange):
            u_val = u_val*np.ones((self.d_u,1))
            constraint_values_u_x[u_idx, :] = sdyn_problem.\
                            constraint_function_values(u_val, x0, W_time_step)
            if (any(x<0 for x in constraint_values_u_x[u_idx, 0:6])):
                constraint_flag[u_idx] = 0
        violated = constraint_flag[0]|constraint_flag[1]
        return violated

    def step_singleX(self, x0, sdyn_problem, fap, W_time_step):
        """
        Implement a step of value iteration algorithm        

        :param x_val: Initial values of state space points
        :param sdyn_problem: Stochastic dynamic problem class
        :param fap: GP based function approximator for value function
        :param W_time_step: Samples of uncertain parameters at evaluation time

        Return optimal value function approximator at each point in x_val
        """
        violation = 0
        unknown_violation = 0
        # Choose u0 that will never violate the upper bound of input u
        u0 = 0.1 * np.ones((self.d_u, 1)) 
        
        g = lambda u, x, t, i: \
                sdyn_problem.constraint_function_values(u, x, t)[i]
                
        g_der = lambda u, x, t, i: \
                sdyn_problem.constraint_function_values_derivative()[i]
        
        g_ = lambda u, x, t, i: \
                sdyn_problem.constraint_function_values(u, x, t)
        
        obj_func = lambda u, x, fap, t: sdyn_problem.bellman_rhs(u, x, fap, t)
        obj_func_der = lambda u, x, fap, t: sdyn_problem.bellman_rhs_derivative(u, x, fap, t)
        
        n_constraints = 7
        upper_bound = 1e-2*sdyn_problem.least_max_input(x0, W_time_step)- 0.001
                      
        constraints = [{'type':'ineq',
                       'fun': g,
                       'args': (x0, W_time_step, i)}
                       for i in range(n_constraints)]
                       
        constraints_ = [{'type':'ineq',
                       'fun': g,
                       'jac': g_der,
                       'args': (x0, W_time_step, i)}
                       for i in range(n_constraints)]

        # constraints_ = {'type':'ineq',
                       # 'fun': g_,
                       # 'args': (x0, W_time_step)}
                       
        options_ = {'maxiter':100}             

        temp_constraints_violated = self.check_violations(x0\
            , sdyn_problem, fap, W_time_step)

        # http://lagrange.univ-lyon1.fr/docs/scipy/0.17.1/generated/scipy.optimize.minimize.html
        if temp_constraints_violated == 1:
            res = minimize(obj_func,
                       u0,
                       args=(x0, fap, W_time_step),
                       jac=obj_func_der,#None
                       bounds=[(0., upper_bound)],
                       constraints=constraints_,#constraints
                       options=options_,
                       method='SLSQP')

            # print "objective, u, message, nfev, nit:", res['fun'], res['x'], res['message'], res['nfev'], res['nit']
            # Handling the case of still no feasible solution, if that happens
            if res['success'] == False:
                # print "SLSQP somehow still failed"
                # print "Constraints:", sdyn_problem.\
                        # constraint_function_values(res['x'], x0, W_time_step)

                VF_opt, u_opt = self.value_iteration_step_unconstrained_solve_and_check\
                            (x0, sdyn_problem, fap, W_time_step)
                opt_dict = {'VF':VF_opt, 'INP':u_opt, 'status':True}
                unknown_violation = 1
                # quit()
            else:
                # print "SLSQP found", res['x'], res['success']
                if (np.isnan(res['x']) == True):
                    quit()
                opt_dict = {'VF':res['fun'], 'INP':res['x'], 'status':True}

        else:
            VF_opt, u_opt = self.value_iteration_step_unconstrained_solve_and_check\
                            (x0, sdyn_problem, fap, W_time_step)
            opt_dict = {'VF':VF_opt, 'INP':u_opt, 'status':True}
            violation = 1

        single_dict = {'OD':opt_dict, 'V':violation, 'UV':unknown_violation}

        # return opt_dict, violation, unknown_violation
        return single_dict

    # Execute a step of value iteration: Discretize and solve
    def value_iteration_step_unconstrained_solve_and_check(self, x_val, sdyn_problem, fap, W_time_step):

        """
        :param x_val: A point in state space domain
        :param sdyn_problem: Stochastic dynamic problem class
        :param fap: GP based function approximator for value function
        :param W_time_step: Samples of uncertain parameters at evaluation time

        Run a multi-start (from 0k to 35k) with different input values for a value of x0, choose minimum u_val that satisfies constraints
        If none do, then resort to minimax control
        """
        x0 = x_val
        # print "X value tested", x0
        allowable_u = sdyn_problem.least_max_input(x0, W_time_step)
        pts_in_grid = int(np.floor(allowable_u))
        urange = 1e-2*np.array(range(pts_in_grid))
        optim_urange = []
        optim_urange_val_func = []
        constraint_flag = [1] * (pts_in_grid)
        constraint_max_temp = [1] * (pts_in_grid)
        constraint_min_temp = [1] * (pts_in_grid)
        constraint_input = [1] * (pts_in_grid)
        constraint_values_u_x = np.zeros(((pts_in_grid), 7))
        
        fwd_diff = range(pts_in_grid)
        analytical_grad = range(pts_in_grid)

        for u_idx, u0 in enumerate(urange):
            u0 = u0*np.ones((self.d_u,1))
            u0 = u0[0]
            
            # Compute objective function
            objective_value = sdyn_problem.bellman_rhs\
                                (u0, x0, fap, W_time_step)
            constraint_values_u_x[u_idx, :] = sdyn_problem.\
                            constraint_function_values(u0, x0, W_time_step)


            # print "constraint at", u0, "is ", constraint_values_u_x[u_idx, :],\
            # "along with value function", objective_value
            
            """
            Check whether constraint is satisfied: Hard change in value: can be changed to derived value from constraint invalidation
            Assumption: System can either get overheated or overcooled
            Overheat implies u_optimal 0, Overcooled implies u_optimal <35k(whatever allowed)
            """

            # Save value function and optimal input only if it satisfies all constraints
            if (any(x<0 for x in constraint_values_u_x[u_idx, :])):
                constraint_flag[u_idx] = 0
            else:
                optim_urange_val_func.append(objective_value)
                optim_urange.append(u0[0])
                
            if (any(x<0 for x in constraint_values_u_x[u_idx, 0:3])):
                constraint_max_temp[u_idx] = 0
            if (any(x<0 for x in constraint_values_u_x[u_idx, 3:6])):
                constraint_min_temp[u_idx] = 0
        
        # Constraint handling: If all max_temp/min_temp constraints are not satisfied (flag == 0), then replace with optimal u_val 0 or u_feasible_max
        # print constraint_input
        # max_feasible_u_index = constraint_input.index(0) - 1
        try:
            max_feasible_u_index = constraint_input.index(0) - 1
        except ValueError:
            max_feasible_u_index = pts_in_grid - 1
        
        # print "Constraint flags are", constraint_input, max_feasible_u_index#constraint_max_temp, constraint_min_temp, 
        # print "Optimal values for given input are", optim_urange_val_func, optim_urange

        if (sum(constraint_flag) == 0):

            if (sum(constraint_max_temp) == 0):

                u_val = 0.
                val_func = sdyn_problem.bellman_rhs\
                                ([0.], x0, fap, W_time_step)

            elif (sum(constraint_min_temp) == 0):    

                u_val = urange[max_feasible_u_index]
                val_func = sdyn_problem.bellman_rhs\
                    ([urange[max_feasible_u_index]], x0, fap, W_time_step)

            else:
                print "Let's see if this logic can fail"
                quit()

        else:
            optimal_index = np.argmin(optim_urange_val_func)
            val_func =  optim_urange_val_func[optimal_index]
            u_val = optim_urange[optimal_index]
        
        return np.array([val_func]), np.array([u_val])

if __name__ == '__main__':
    print 'Step'

    comm = mpi.COMM_WORLD
    plan_horizon = 48
    d_x = 43
    d_u = 1
    d_w = 9
    n_collector = 13
    beta = 0.9
    eval_pts = 500
    instances = 10

    control_outputs = ValueIterationAlgorithm(d_x=d_x, d_u=d_u, d_w=d_w,\
         n_w = instances, eval_pts=eval_pts, plan_horizon=plan_horizon, tol=1e-5)
    
    Tc_t=np.random.rand(3,n)+5
    x_t=np.zeros((43,1))
    x_t[0]=20
    x_t[1]=20
    x_t[2]=22
    x_t[3]=30
    x_t[4:]=np.reshape(Tc_t, (3*n, 1))

    ValueIterationStep_ = ValueIterationStep(d_x=d_x, d_u=d_u, eval_pts=eval_pts)

    with open('simulated_inputs_1483595258.79.pkl', 'rb') as f:
        VF_x_horizon = pickle.load(f)
        u_opt_x_horizon = pickle.load(f)
        X = pickle.load(f)

    val_func = VF_x_horizon[:, 11]
    input_dimension = 4
    fap = GaussianProcessApproximator(input_dimension,
                                      fixed_noise=None,
                                      comm=comm,
                                      verbose=0)
    X = np.abs(100*np.random.rand(eval_pts, input_dimension))
    outputs = [{'obj': [val_func[idx]]} for idx, x in enumerate(X)]
    fap.fit(X[:, 0:4], outputs)

    sdyn_problem = StochasticDynProgProblem(d_x=d_x, d_u=d_u, d_w=d_w, n_collector=n_collector, beta=beta, n_w=instances)

    time_step = 11
    W = control_outputs.load_uncertain_val(time_step)
    ValueIterationStep_.value_iteration_step_unconstrained_solve_and_check(x_t, sdyn_problem, fap, W_time_step)