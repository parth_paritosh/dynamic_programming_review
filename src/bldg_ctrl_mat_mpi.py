import numpy as np
import math
import matplotlib.pyplot as plt
import GPy
from scipy.optimize import minimize
import pandas as pd
import os
from matplotlib.font_manager import FontProperties
import time
import cPickle as pickle
import itertools

from lib import func_approximator
from lib import _mpi
from lib import stochastic_dynamics_building_control as stochastic_dynamics# import stochastic_dynamics.StochasticDynProgProblem

# Select between pyOpt and scipy.minimize
# from lib import pyOpt_step_value_iteration_building_control as step_vi# import step_vi.ValueIterationStep
from lib import step_value_iteration_building_control as step_vi# import step_vi.ValueIterationStep
from lib import load_uncertain # import load_uncertain.LoadUncertainParameters

# Value iteration alogrithm class contains only the algorithm and knowledge of problem dimensions
class ValueIterationAlgorithm(step_vi.ValueIterationStep, _mpi.DistributedObject):
    def __init__(self, d_x=43, d_u=1, d_w = 9, n_w = 10, eval_pts=100, \
                        plan_horizon=24, tol=1e-5, comm=None, verbose=0):
        self.d_x = d_x
        self.d_u = d_u
        self.d_w = d_w
        self.n_w = n_w
        self.eval_pts = eval_pts
        self.plan_horizon = plan_horizon
        self.tol = tol
        _mpi.DistributedObject.__init__(self, comm=comm, verbose=0)
    
    # VIA execution with relative convergence condition on value function
    # Replacing X_val with one being generated at each step
    def value_iteration(self, sdyn_problem, load_uncertain, fap, X, start_time):
        """
        Implement value iteration algorithm for building control problem

        :param sdyn_problem: Stochastic dynamic problem class
        :param fap: GP based function approximator for value function
        :param X: Design points
        :param start_time: The time from which ADP simulation is implemented over plan horizon
        :param pred_plot: Class having plotting function for VF checks (Removed)

        Return optimal value function approximator and associated inputs
        """
        VF_x_horizon = np.zeros((self.eval_pts, self.plan_horizon))
        u_opt_x_horizon = np.zeros((self.eval_pts, self.plan_horizon))
        x_val = X
        index = 0

        for index in range(self.plan_horizon):
            prev_time = time.time()
            time_step = self.plan_horizon + start_time - index -1
            # Input index because the data is provided as the simulation starts
            W_time_step = load_uncertain.load_uncertain_val\
                            (self.plan_horizon - index -1)
            if (self.check_schedule(time_step) == 0):
                opt_iteration= self.step(x_val, sdyn_problem, fap, W_time_step)
            else:
                opt_iteration = self.zero_step(x_val, sdyn_problem, fap, W_time_step)
            
            val_func = [i['VF'] for i in opt_iteration if i['status']==True]
            u_val = [i['INP'] for i in opt_iteration if i['status']==True]
            current_time = time.time()
            rank = comm.Get_rank()
            if (rank == 0):
                print "Current step and time taken", time_step,\
                    "and", current_time - prev_time

            # Train OVF GP named fap here
            X = [x_val[idx] for idx, i in enumerate(opt_iteration) if\
                 i['status']==True]
            X = np.vstack(X)
            outputs = [{'obj': val_func[idx]} for idx, x in enumerate(X)]
            # print outputs, X[:, 0:4]
            input_dimension = 4
            fap = func_approximator.GaussianProcessApproximator(input_dimension,
                    fixed_noise=None,
                    comm=self.comm,
                    verbose=0)
            fap.fit(X[:, 0:4], outputs)
            # Values are saved corresponding to time step in future for which it was solved

            total_val_func = [i['VF'] for i in opt_iteration]
            total_u_val = [i['INP'] for i in opt_iteration]

            VF_x_horizon[:, time_step - start_time] = total_val_func
            u_opt_x_horizon[:, time_step - start_time] = total_u_val
        return VF_x_horizon, u_opt_x_horizon, fap

    def check_schedule(self, time_step):
        status = 0
        day_time = time_step % 24
        if ((day_time<5) or (day_time>19)):
            status = 1
        return status

    # Execute a step of value iteration
    # Try out custom minimizers http://docs.scipy.org/doc/scipy/reference/generated/scipy.optimize.minimize.html
    def step(self, x_val, sdyn_problem, fap, W_time_step):
        """
        Implement a step of value iteration algorithm        

        :param x_val: Initial values of state space points
        :param sdyn_problem: Stochastic dynamic problem class
        :param fap: GP based function approximator for value function
        :param W_time_step: The samples of uncertain parameters at time step of evaluation

        Return optimal value function approximator at each point in x_val
        """
        optimal_inputs = []
        total_violations = 0
        total_unknown_violations = 0

        # Parallelized implementation
        
        my_jobs = func_approximator.distributed_xrange(len(x_val), comm)
        my_out = [self.step_singleX(x_val[i], sdyn_problem, fap, W_time_step)\
                                     for i in my_jobs]
        list_single_dict = list(itertools.chain(*self.comm.allgather(my_out)))

        optimal_inputs = [i['OD'] for i in list_single_dict]
        V = [i['V'] for i in list_single_dict]
        UV = [i['UV'] for i in list_single_dict]

        total_violations = sum(V)
        total_unknown_violations = sum(UV)
        
        if (comm.Get_rank() == 0):  
            print "Total violations and 'unknown reason violations' are:", total_violations, total_unknown_violations
        return optimal_inputs
    
    def zero_step(self, x_val, sdyn_problem, fap, W_time_step):
        """
        Return zero inputs and function approximators
        """
        optimal_inputs = []
        total_violations = 0
        total_unknown_violations = 0

        my_jobs = func_approximator.distributed_xrange(len(x_val), comm)
        my_out = [sdyn_problem.bellman_rhs([0.], x_val[i], fap, W_time_step)\
                                     for i in my_jobs]
        val_func_list = list(itertools.chain(*self.comm.allgather(my_out)))
        
        u_val = 0.
        for idx, eval_pt in enumerate(x_val):
            opt_dict = {'VF':val_func_list[idx], 'INP':np.array(u_val), 'status':True}
            optimal_inputs.append(opt_dict)

        return optimal_inputs

if __name__ == '__main__':

    with open('simulation_data.pkl', 'rb') as f: 
        plan_horizon, d_x, d_u, d_w, n_collector, beta,\
        eval_pts, instances, UNCERTAIN_INPUT_FILENAME,\
        WEATHER_FILE, OUTPUT_FILENAME, pump_type = pickle.load(f)

    start_time = [x.strip() for x in WEATHER_FILE.split("_")][-1]
    start_sim_time = int(start_time)

    from mpi4py import MPI as mpi
    comm = mpi.COMM_WORLD
    verbose = 0

    bldg_control = stochastic_dynamics.StochasticDynProgProblem(d_x=d_x, d_u=d_u, d_w=d_w, n_collector=n_collector, beta=beta, n_w=instances, pump_type = pump_type)

    load_uncertain = load_uncertain.LoadUncertainParameters(d_w=d_w, n_w=instances)

    control_outputs = ValueIterationAlgorithm(d_x=d_x, d_u=d_u, d_w=d_w,\
         n_w = instances, eval_pts=eval_pts, plan_horizon=plan_horizon, tol=1e-5, comm=comm, verbose=verbose)

    input_dimension = 4

    rank = comm.Get_rank()
    if (rank == 0):
        verbose = 1

    fap = func_approximator.GaussianProcessApproximator(input_dimension,
                                      fixed_noise=None,
                                      comm=comm,
                                      verbose=0)
    X_ = np.abs(100*np.random.rand(eval_pts, input_dimension))
    outputs = [{'obj': [0.]} for x in X_]
    fap.fit(X_[:, 0:4], outputs)

    # Obtain a 500 point space generated via Latin hypercube sampling for room and tank temperature
    with open('DP_state_space.p', 'r') as f:
        X = pickle.load(f)

    rank = comm.Get_rank()
    if (rank == 0):
        print "--------------------- BEGIN -------------------------"
        print "Number of points on which function is evaluated:", eval_pts
        print "simulation time is ", start_sim_time
    start_time = time.time()
    VF_x_horizon, u_opt_x_horizon, fap = \
        control_outputs.value_iteration(bldg_control, load_uncertain, \
                                        fap, X, start_sim_time)
    
    if (rank == 0):
        print "Value function run takes time", time.time() - start_time
        print np.shape(VF_x_horizon), np.shape(u_opt_x_horizon), np.shape(X)

        OUTPUT_FILENAME = OUTPUT_FILENAME+".p"
        result_dict = {'VF_x_horizon':VF_x_horizon,'u_opt_x_horizon':u_opt_x_horizon,'X':X}
        pickle.dump( result_dict, open( OUTPUT_FILENAME, "wb" ) )
