import cPickle as pickle


def create_simulation_input(date_hour, plan_horizon, INSTANCES):
    """
    Create simulation_data.pkl file containing the file names

    :param date_hour: Date and hour for which simulation would be run
    
    Return pkl file
    """    
    plan_horizon = plan_horizon
    d_x = 43
    d_u = 1
    d_w = 9
    n_collector = 13
    beta = 1.
    pump_type = 1

    EVALUATION_POINTS = 500
    INSTANCES = INSTANCES

    # Uncertainty file has number of samples and horizon
    # Can also be made to self generate and get saved
    UNCERTAIN_INPUT_FILENAME = date_hour+'_irr_'+str(INSTANCES)+'_'+str(plan_horizon)+'.pcl'
    WEATHER_FILE = date_hour

    # Include DP/MPC, samples, horizon
    OUTPUT_FILENAME = date_hour+'_DP_'+str(plan_horizon)+"_"+str(INSTANCES)

    # Ensuring the minimum value of irradiation is chosen
    with open('./simulation_data.pkl', 'wb') as f:
        pickle.dump([plan_horizon, d_x, d_u, d_w, n_collector, beta,\
                EVALUATION_POINTS, INSTANCES, UNCERTAIN_INPUT_FILENAME,\
                WEATHER_FILE, OUTPUT_FILENAME, pump_type], f)

if __name__ == '__main__':
    year = 2017
    start_day = [1, 16] #mm/dd FORMAT
    end_day = [1, 17]

    current_day = [1, 18]
    month = current_day[0]
    date = current_day[1]

    hour = 0
    
    plan_horizon = 24
    INSTANCES = 100
    date_hour = str(year)+'_'+str(month)+'_'+str(date)+'_'+str(hour)
    print date_hour
    create_simulation_input(date_hour, plan_horizon, INSTANCES)