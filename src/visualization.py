import numpy as np
import math
import matplotlib.pyplot as plt
import GPy
import design
from scipy.optimize import minimize
import os
from scipy.ndimage.filters import gaussian_filter
import cPickle as pickle
from matplotlib.font_manager import FontProperties
from mpi4py import MPI as mpi

comm = mpi.COMM_WORLD

from lib import func_approximator
from lib import stochastic_dynamics_building_control as stochastic_dynamics# import stochastic_dynamics.StochasticDynProgProblem
from lib import load_uncertain # import load_uncertain.LoadUncertainParameters
from lib import visualization_bldg_ctrl_DP as vis_dp

with open('./simulation_data.pkl', 'rb') as f: 
    plan_horizon, d_x, d_u, d_w, n_collector, beta,\
    EVALUATION_POINTS, INSTANCES, UNCERTAIN_INPUT_FILENAME,\
    WEATHER_FILE, OUTPUT_FILENAME, pump_type = pickle.load(f)
    
    bldg_control = stochastic_dynamics.StochasticDynProgProblem(d_x=d_x, d_u=d_u, d_w=d_w, n_collector=n_collector, beta=beta, n_w=INSTANCES)
    load_uncertain_params = load_uncertain.LoadUncertainParameters(d_w=d_w, n_w=INSTANCES)

    file_path = OUTPUT_FILENAME+".p"
    with open(file_path) as f:
        result_dict = pickle.load(f)
        VF_x_horizon = result_dict['VF_x_horizon']
        u_opt_x_horizon = result_dict['u_opt_x_horizon']
        X = result_dict['X']

    print np.shape(X), np.shape(u_opt_x_horizon), np.shape(VF_x_horizon)
    # print "X values are", X[:, 0:4]
    # print VF_x_horizon[:, 11], u_opt_x_horizon[:, 11]
    val_func = VF_x_horizon[:, 11]

    input_dimension = 4
    fap = func_approximator.GaussianProcessApproximator(input_dimension,
                                      fixed_noise=None,
                                      comm=comm,
                                      verbose=0)
    outputs = [{'obj': [val_func[idx]]} for idx, x in enumerate(X)]
    fap.fit(X[:, 0:4], outputs)
    
    pred_plot = vis_dp.plotting(d_x = 43, plan_horizon = np.shape(VF_x_horizon)[1])   

    # pred_plot.scatter_design_points(X)

    # pred_plot.plot_time_dependent(load_uncertain_params, plan_horizon)
    # pred_plot.plot_irradiation_samples(load_uncertain_params)
    # pred_plot.plot_heatmap(u_opt_x_horizon)

    # Plot optimal input function
    # pred_plot.predict_plot_policy(u_opt_x_horizon, X)
    # pred_plot.predict_plot_policy_mesh(u_opt_x_horizon, X)
    # pred_plot.predict_plot_VF(VF_x_horizon, X)
    pred_plot.predict_plot_VF_contour(VF_x_horizon, X)
    
    # pred_plot.VF_grid(VF_x_horizon, X)
    
    # pred_plot.VF_grid_tank(VF_x_horizon, X)
    
    # pred_plot.VF_grid_tank_mesh(VF_x_horizon, X)
    # pred_plot.VF_grid_tank_contour(VF_x_horizon, X)
    
    pred_plot.VF_grid_tank_contour_4(VF_x_horizon, X)
    
    # pred_plot.simulate_trajectory_plots(bldg_control, load_uncertain_params, X, u_opt_x_horizon)
    plt.show()