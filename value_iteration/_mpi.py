"""
Some useful parallelization methods/classes.

"""


__all__ = ['DistributedObject', 'distributed_xrange']


import numpy as np


class DistributedObject(object):

    """
    An object that is aware of the parallelization environment.

    :param comm:        An MPI communicator or ``None``.
    :param verbose:     The verbosity level desired. It will be automatically
                        be zero if the rank of the task storing the objecti
                        is not zero.
    """

    @property
    def use_mpi(self):
        """
        Are we using MPI or not?
        """
        return self.comm is not None

    @property
    def verbose(self):
        if self.rank == 0:
            return self._verbose
        else:
            return 0

    def __init__(self, comm=None, verbose=0):
        self.comm = comm
        self._verbose = verbose
        if self.use_mpi:
            self.rank = comm.Get_rank()
            self.size = comm.Get_size()
        else:
            self.rank = 0
            self.size = 1


def distributed_xrange(n, comm=None):
    """
    If ``comm`` is a MPI communicator, then we distributed the range to
    the processors as evenly as possible.
    """
    if comm is None:
        return xrange(n)
    rank = comm.Get_rank()
    size = comm.Get_size()
    r = n % size
    my_n = n / size
    if rank < r:
        my_n += 1
    my_start = rank * my_n
    if rank >= r:
        my_start += r
    my_end = my_start + my_n
    return xrange(my_start, my_end)
