from simulation_input_data_week import create_simulation_input
import time
import os
import subprocess
days_in_month = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
# The file creates multiple simulation_inputs.pkl from start to end day and runs them
# Only runs for continuous segment in an year; Can be extended across years

year = 2017
start_day = [1, 16] #mm/dd FORMAT
end_day = [1, 17]

month = start_day[0]
date = start_day[1]

plan_horizon=24

INSTANCES=100

while not ((month == end_day[0]) and (date == end_day[1]+1)):

    if date == days_in_month[month-1]+1:
        month = month + 1
        date = 1
    for hour in range(24):
        date_hour = str(year)+'_'+str(month)+'_'+str(date)+'_'+str(hour)
        create_simulation_input(date_hour, plan_horizon, INSTANCES)
        print date_hour
        
        # Send the files to cluster, download results and rename
        OUTPUT_FILENAME = date_hour+'_DP_'+str(plan_horizon)+"_"+str(INSTANCES)+'.p'
        subprocess.call('sh transfer_run_pbs.sh')
        subprocess.call('sh check_file_exists.sh %s' % OUTPUT_FILENAME)
        
        # Add a python file to check whether the file has downloaded in function form
        # time.sleep(72)
    date = date + 1