"""
Import building control problem class: 
Relevant functions are State_space, constraints, cost_function(J)
State_space(x, u, v, w)
Cost_function(x, u, v, w)
constraints (x, u, v, w)
"""
import numpy as np
import math
import GPy
from scipy.optimize import minimize
from building_control import DynamicProgrammingProblem
import os
import sys
from mpi4py import MPI as mpi

from func_approximator import *
from load_uncertain import LoadUncertainParameters

comm = mpi.COMM_WORLD


class StochasticDynProgProblem(DynamicProgrammingProblem):
    """
    Define dynamic programming problem with expectation over stochasticity

    :param d_x: States: Temperature & Collector
    :param d_u: Inputs
    :param d_w: Time varying uncertainty
    :param n_collector: Dimensionality of solar collector model
    """

    def __init__(self, d_x=43, d_u=1, d_w=9, n_collector=13, beta=0.9, n_w=1, pump_type=1):
        super(StochasticDynProgProblem, self).__init__(d_x=d_x,
                                                       d_u=d_u,
                                                       d_w=d_w,
                                                       n_collector=n_collector,
                                                       pump_type=pump_type)
        # Initialize variables common to problem class
        # beta value 0.07 after 24 steps
        self.beta = beta 
        self.n_w = n_w
        
    
    # Deterministic function definition
    def f(self, x = None, u = None, w = None):
        """
        Evaluate the right hand side of the dynamics.

        :param x: Current state
        :param u: Controller input
        :param w: Instance of stochastic inputs
        """
        # Input x(d_x, 1), u(d_u, 1), w(d_w, 1)
        x_next = self.State_Space(x, u, w)
        return x_next
        
    def f_flat(self, x=None, u=None, w=None):
        x_next = self.State_Space(x, u, w)
        return x_next.flatten()
        
    def states(self, x, u, W_time_step):
        """
        Output: array(of arrays) of possible states w.r.t. uncertain variables

        :param x: Current state
        :param u: Controller input
        :param W_time_step: Samples of uncertain parameters at evaluation time
        """
        X_k1 = [[] for i in range(self.n_w)]
        # Loop over W and compute corresponding next states
        for idx, unc_par_val in enumerate(W_time_step):
            next_X = self.f(x, u, unc_par_val[0])
            X_k1[idx]= next_X.tolist()
        return np.array(X_k1)

    def cost(self, x, u, W_time_step):
        """
        Output: Cost as scalar variable

        :param x: Current state
        :param u: Controller input
        """
        cost_k1 = [[] for i in range(self.n_w)]
        cost_sum = 0.
        for idx, unc_par_val in enumerate(W_time_step):
            next_Xk_cost = self.cost_function(x, u, unc_par_val[0])
            cost_sum = cost_sum + next_Xk_cost
            
            cost_k1[idx]= next_Xk_cost.tolist()
        # print "Cost values are", np.array(np.nansum(cost_k1)), np.array(cost_sum)
        return np.array(cost_sum)

    def cost_derivative(self, x, u, W_time_step):
        """
        Output: Cost as scalar variable

        :param x: Current state
        :param u: Controller input
        """
        cost_sum = 0.
        for idx, unc_par_val in enumerate(W_time_step):
            next_Xk_cost = self.cost_function_derivative(x, u, unc_par_val[0])
            cost_sum = cost_sum + next_Xk_cost
        # print "Cost values are", np.array(np.nansum(cost_k1)), np.array(cost_sum)
        return np.array(cost_sum)
        
    
    def VF_at_x_next(self, fap, x, u, W_time_step):
        """
        Compute expected value function by computing at possible next states   
        VF is cost-to-go that should be minimized

        :param x: Current state
        :param u: Controller input
        :param fap: Function approximator for value fuction
        :param W_time_step: Samples of uncertain parameters at evaluation time
        """
        next_state = self.states(x, u, W_time_step)
        next_state = np.reshape(next_state, (self.n_w, 43))
        # Obtain mean value function at computed next states
        VF_m, VF_var = fap.predict(next_state[:, 0:4])
        return np.sum(VF_m.flatten())

    def VF_derivative_at_x_next(self, fap, x, u, W_time_step):
        """
        Compute sum of gradients of value function by computing at possible next states   
        VF derivatives to be summed up

        :param x: Current state
        :param u: Controller input
        :param fap: Function approximator for value fuction
        :param W_time_step: Samples of uncertain parameters at evaluation time
        """
        next_state = self.states(x, u, W_time_step)
        next_state = np.reshape(next_state, (self.n_w, 43))
        # Obtain mean value function at computed next states
        VF_der = fap.predict_gradients(next_state[:, 0:4])
        # print np.shape(VF_der) is (100L, 4L, 1L)
        VF_der = np.reshape(VF_der, (self.n_w, 4))
        next_state_der = self.state_space_derivatives()[:, None] # 4 x 1 matrix
        sum_VF_der = np.dot(VF_der, next_state_der)
        # sum_VF_der = sum_VF_der[0,:,0]
        sum_VF_der = np.sum( sum_VF_der )
        # print , type(VF_der), np.shape(VF_der), np.sum(VF_der.flatten())
        return sum_VF_der
        
    def bellman_rhs(self, u, x, fap, W_time_step):
        """
        :param u: Input value
        :param x: Point in state space
        :param W_time_step: Samples of uncertain parameters at evaluation time

        Compute the RHS of bellman equation
        """
        assert np.isnan(u[0])==False
        # print "Value function at", x[0: 4], u, "is", self.VF_at_x_next(fap, x, u, time_step)/self.n_w
        
        rhs_eval = (self.cost(x, u, W_time_step) + \
                    self.beta*self.VF_at_x_next(fap, x, u, W_time_step))/self.n_w
                    
        # print "objective, u :", rhs_eval, u
        # print "Constraints:", self.constraint_function_values(u, x, W_time_step)
        if np.isnan(u[0])==True:
            sys.exit()
            quit()
        return rhs_eval

    def bellman_rhs_derivative(self, u, x, fap, W_time_step):
        """
        :param u: Input value
        :param x: Point in state space
        :param W_time_step: Samples of uncertain parameters at evaluation time

        Compute the RHS of bellman equation; rhs_eval_der is float
        """
        assert np.isnan(u[0])==False
        rhs_eval_der = (self.cost_derivative(x, u, W_time_step) + \
                    self.beta*self.VF_derivative_at_x_next(fap, x, u, W_time_step))/self.n_w
        # print "objective_gradient, u :", rhs_eval_der, u

        return [rhs_eval_der]
        
    def constraint_function_values(self, u_k, x_k, W_time_step):
        """
        :param u_k: Input value
        :param x_k: Point in state space
        :param time_step: Time step of evaluation

        Positive values are obtained when constraint is satisfied
        g_i(x) >= 0

        Output: List of constraints in order of x_min, x_max and u_max
        """
        W = W_time_step
        X_k1_min = [[] for i in range(self.n_w)]
        X_k1_max = [[] for i in range(self.n_w)]
        U_max = [[] for i in range(self.n_w)]
        for idx, unc_par_val in enumerate(W):
            # print x_k[0:4], u_k, unc_par_val
            next_constraints = self.constraints(x_k, u_k, unc_par_val[0])
            X_k1_min[idx]= -1*next_constraints[0]
            X_k1_max[idx]= -1*next_constraints[1]
            U_max[idx]= -1*next_constraints[2]
            # print "Constrained input value is", U_max[idx], "Current input value is ", u_k
        # print "x_k1_min", X_k1_min, X_k1_max, U_max    
        # Compute expectation over constraint bounds
        constr_fns_1 = (np.sum(X_k1_min, axis=0))/float(self.n_w)
        constr_fns_2 = (np.sum(X_k1_max, axis=0))/float(self.n_w)
        constr_fns_3 = (np.sum(U_max, axis=0))/float(self.n_w)
        # print constr_fns_3
        constr_fns_1 = constr_fns_1.flatten()
        constr_fns_2 = constr_fns_2.flatten()
        
        """
        Implement constraint widening; +2 min; +3 max
        Not recommended as it merely delays the input requirement
        if (any(x<0 for x in constr_fns_1)):
            constr_fns_1 = constr_fns_1 + 2*np.ones(np.shape(constr_fns_1))
        if (any(x<0 for x in constr_fns_2)):
            constr_fns_2 = constr_fns_2 + 2*np.ones(np.shape(constr_fns_2))
        """

        # Remove the index of dummy constraints
        constr_fcns = np.concatenate((constr_fns_1[1:4], constr_fns_2[1:4])).tolist() 
        try:
            constr_fcns.append(constr_fns_3[0][0])
        except IndexError:
            # SLSQP has single brace
            constr_fcns.append(constr_fns_3[0])

        # print "Printing contraint functions", constr_fcns
        return constr_fcns

    def constraint_function_values_pyOpt(self, u_k, x_k, W_time_step):
        """
        :param u_k: Input value
        :param x_k: Point in state space
        :param time_step: Time step of evaluation

        Negative values are obtained when constraint is satisfied
        g_i(x) <= 0

        Output: List of constraints in order of x_min, x_max and u_max
        """
        constr_values = self.constraint_function_values(u_k, x_k, W_time_step)
        constr_values = [-1*i for i in constr_values]
        # print constr_values
        return constr_values

    def pyOpt_constraint_function_values_derivative(self):
        """
        Negative values are obtained when constraint is satisfied
        g_i(x) <= 0
        """
        constraint_derivatives = self.constraint_function_values_derivative()
        constraint_derivatives = [-1*i for i in constraint_derivatives]

        return constraint_derivatives


    def least_max_input(self, x_k, W_time_step):
        """
        :param u_k: Input value
        :param time_step: Time step of evaluation

        Output: Lowest bound of maximum inputs for different uncertain parameters
        """
        W = W_time_step
        U_max = [[] for i in range(self.n_w)]

        for idx, unc_par_val in enumerate(W):
            next_constraints = self.u_t_max(x_k, unc_par_val[0])
            U_max[idx]= next_constraints
        # Compute expectation over constraint bounds
        # average_maximum_input = (np.sum(U_max, axis=0))/float(self.n_w)
        least_maximum_input = np.min(U_max, axis = 0)
        return least_maximum_input

if __name__ == '__main__':
    print 'RUNNING'
    plan_horizon = 24
    d_x = 43
    d_u = 1
    d_w = 9
    n_collector = 13
    beta = 0.9
    eval_pts = 500
    instances = 100
    dyn_prog = StochasticDynProgProblem(d_x=d_x, d_u=d_u, d_w=d_w, \
        n_collector=n_collector, beta=beta, n_w=instances)
    load_uncertain = LoadUncertainParameters(d_w=d_w, n_w=instances)

    n =13
    Tc_t=np.random.rand(3,13)+5
    x_t=np.zeros((43,1))
    x_t[0]=20
    x_t[1]=20
    x_t[2]=22
    x_t[3]=30
    x_t[4:]=np.reshape(Tc_t, (3*n, 1))
    u_t=30*1000
    v_t=np.array([5, 1800, 2, 2, 12, 100, 500, 50, 400])
    W_time_step = load_uncertain.load_uncertain_val(2)
    print dyn_prog.constraint_function_values(np.asarray([0.25]), x_t, W_time_step)     
    print dyn_prog.least_max_input(x_t.flatten(), W_time_step) # gives 25.832

    eval_pts = 20
    input_dimension = 4
    fap = GaussianProcessApproximator(input_dimension,
                                      fixed_noise=None,
                                      comm=comm,
                                      verbose=0)
    X = np.abs(100*np.random.rand(eval_pts, input_dimension))
    outputs = [{'obj': [0.]} for x in X]
    fap.fit(X[:, 0:4], outputs)        
    print dyn_prog.bellman_rhs(u = [0.000000000003], x = x_t, fap = fap, W_time_step = W_time_step)