#############################################################
##### Stochastic dynamic problem definition building control#
##### Parth Paritosh                                        #
#####                                                       
#############################################################


import numpy as np
import math
import matplotlib.pyplot as plt
import GPy
import warnings
from mpi4py import MPI as mpi

## Global variables
MAX_HEATING_POWER = 100

class DynamicProgrammingProblem(object):
    def __init__(self, d_x = 4, d_u = 1, d_w = 9, n_collector = 13, pump_type = 1):
        self.d_x = d_x
        self.d_u = d_u
        self.d_w = d_w
        self.n_collector = n_collector
        self.pump_type = pump_type
        down_scale = 1e-5
        up_scale = 1e5
        self.down_scale = down_scale
        self.up_scale = up_scale
        
    # convert_deg_to_rad
    def deg2rad(self, deg):
        rad=deg*np.pi/180
        return rad

    # get_output_of_solar_model
    def Solar_split(self, Time, doy, GHI, GHI_clr, Tdb, RH, Longitude_st=75., Longitude_loc=86.15, Lattitude=39.79, beta=90, psi=0):

        # Reindl's solar model, calculate direct and diffuse irradiance on any given surface

        # GHI: unit w/m2;
        # Tdb: unit C;
        # RH: unit %;
        # Longitude_st: longitude at standard merridian, degree;
        # Local longitude: degree;
        # Lattitude: local lattitude, degree;
        # beta: surface slope, degree;
        # phi: surface orientation, south=0, west=90, north=180, east=270


        # =========== Calculate solar time and hour angle

        B=360*(doy-81)/364
        Et=9.87*np.sin(self.deg2rad(2*B))-7.53*np.cos(self.deg2rad(B))-1.5*np.sin(self.deg2rad(B))
        SolarTime=Time+4*(Longitude_st-Longitude_loc)/60+Et/60
        H=(SolarTime-12+0.5)*15   #hour angle
        delta=23.45*np.sin(self.deg2rad(360*(284+doy)/365))           #declination
        # solar altitude
        alpha=np.arcsin(np.cos(self.deg2rad(Lattitude))*np.cos(self.deg2rad(delta))*np.cos(self.deg2rad(H))+np.sin(self.deg2rad(Lattitude))*np.sin(self.deg2rad(delta)))
        # solar azimuth
        phi=np.arccos((np.sin(alpha)*np.sin(self.deg2rad(Lattitude))-np.sin(self.deg2rad(delta)))/(np.cos(alpha)*np.cos(self.deg2rad(Lattitude))))*H/np.abs(H)
        # angle of incidence
        theta=np.arccos(np.cos(alpha)*np.cos(np.abs(phi-self.deg2rad(psi)))*np.sin(self.deg2rad(beta))+np.sin(alpha)*np.cos(self.deg2rad(beta)))


        Esc = 1367        # Solar constant, w/m2.
        rou_g = 0.2       # Ground reflectance

        # =========== Get the day of the year and obtain correlation factor during to
        # solar-earth distance.
        b = 2 * 180 * doy / 365
        Rav_R=1.00011+0.034221*np.cos(self.deg2rad(b))+0.00128*np.sin(self.deg2rad(b))+0.000719*np.cos(self.deg2rad(2*b))+0.000077*np.sin(self.deg2rad(2*b))

        # Extraterrestrial solar
        Ea = Esc * Rav_R

        # ============== Sky clearness index !! Alternative approach
        SkyClr = GHI / GHI_clr

        # ============== If GHI = 0 or sind(alpha)<0, all outputs are 0.


        # IT total surface irradiance: W/m2;
        # IbT direct irradiance on the surface: W/m2;
        # IdT diffuse irradiance on the surface: W/m2;
        # IgT ground reflection to the surface: W/m2

        if GHI< 5. or np.sin(alpha) <=0.:
            IT  = 0.
            IbT = 0.
            IdT = 0.
            SkyClr = 0.


        # ============== Obtain diffuse irradiance fraction (Mode 2 of TRNSYS).
        if SkyClr <= 0.3:
            frac_dif = 1 - 0.232 * SkyClr + 0.0239 * np.sin(alpha) - 0.000682 * Tdb + 0.0195 * RH / 100
            frac_dif = min(frac_dif, 1)
        elif SkyClr > 0.3 and SkyClr <= 0.78:
            frac_dif = 1.329 - 1.716 * SkyClr + 0.267 * np.sin(alpha) - 0.00357 * Tdb + 0.106 * RH / 100
            frac_dif = min(frac_dif, 0.97)
            frac_dif = max(frac_dif, 0.1)
        else:
            frac_dif = 0.426 * SkyClr - 0.256 * np.sin(alpha) + 0.00349 * Tdb + 0.0734 * RH / 100
            frac_dif = max(frac_dif, 0.1)


        HI_dif = GHI * frac_dif
        HI_beam = GHI * (1 - frac_dif)
        Rb = np.cos(theta) / np.cos(np.pi/2-alpha)
        Rr = 0.5 * (1 - np.cos(self.deg2rad(beta))) * rou_g
        Ai = HI_beam / np.sin(alpha) / Ea   # Anisotropy index.
        IdT = HI_dif * (0.5 * (1 - Ai) * (1 + np.cos(self.deg2rad(beta))) + Ai * Rb)

        
        if HI_beam * Rb>0:
            IbT=HI_beam * Rb
        else:
            IbT=0.
        
        if GHI * Rr>0:
            IgT=GHI * Rr
        else:
            IgT=0
            
        if IdT>0:
            IdT+=IgT
        else:
            IdT=0.+IgT
            
        
        # ================ Total radiation W/m2
        IT  = IbT + IdT
        
        # ============= the diffuse irradiance transmitted through the window, W/m2
        tau_d=0.228  # window transmittance of diffuse radiation
        IdT=IdT*tau_d

        # ============= the beam irradiance transmitted through the window, W/m2
        if theta>0 and theta<5:
            IbT=0.287*IbT
        elif theta>5 and theta<15:
            IbT=0.288*IbT
        elif theta>15 and theta<25:
            IbT=0.283*IbT
        elif theta>25 and theta<35:
            IbT=0.276*IbT
        elif theta>35 and theta<45:
            IbT=0.266*IbT
        elif theta>45 and theta<55:
            IbT=0.247*IbT
        elif theta>55 and theta<65:
            IbT=0.206*IbT
        elif theta>65 and theta<75:
            IbT=0.134*IbT   
        elif theta>75 and theta<85:
            IbT=0.048*IbT
        elif theta>85 and theta<90:
            IbT=0*IbT
        else: 
            IbT=0*IbT
                
        Itot=IbT+IdT


        if Itot>=170:
            IbT=IbT*0.15
            IdT=IdT*0.15
        else:
            IdT=IdT
            IbT=IbT

                    
        # ============total after window and shade
        Igain=IbT+IdT
        
        # =============GEOSURF of envelopes
        GEOSURF_N=0.07
        GEOSURF_S=0.01
        GEOSURF_W=0.1
        GEOSURF_E=0.1
        GEOSURF_R=0.32
        GEOSURF_F=0.4

        # =============window area
        A_w=44.5

        # =============optical properties of envelopes
        a_N=0.4
        a_S=0.023
        a_W=0.4
        a_E=0.4
        a_R=0.6
        a_F=0.6

        Qsg1_beam=IbT*A_w*(GEOSURF_N*a_N+GEOSURF_S*a_S+GEOSURF_W*a_W+GEOSURF_E*a_E+GEOSURF_R*a_R)
        Qsg3_beam=IbT*A_w*GEOSURF_F*a_F

        # ===============diffuse fraction
        fra_N=0.1
        fra_S=0
        fra_W=0.137
        fra_E=0.137
        fra_R=0.313
        fra_F=0.313

        Qsg1_diff=IdT*A_w*(fra_N++fra_S+fra_W+fra_E+fra_R)
        Qsg3_diff=IdT*A_w*fra_F

        Qsg1=Qsg1_beam+Qsg1_diff

        if Qsg1>0:
            Qsg1=Qsg1+2500

        Qsg3=Qsg3_beam+Qsg3_diff;
        if Qsg3<500:
            Qsg3=Qsg3*2.5
        elif Qsg3>1200:
            Qsg3=Qsg3*0.7

        
        return Qsg1, Qsg3, IT
    
    def collector_dynamics_half(self, Tc_t, IT, T_o, v, T_dp, TIME, doy):

        # Tc_t is a (3,n_collector) vector that contains information on all the current temperature of all nodes on the collector

        # modifiable parameters
        H=9.754             # H, m
        L=13.4              # Length, m
        wavelength=0.15     # Wavelength, m
        amplitude=0.035     # Amplitude, m
        crest=0.082         # Crest, m
        slopes=0.03848      # Slope, m
        V_s=0.015           # suction velocity, m/s
        alpha_ap=0.95       # alpha_ap
        L_ap=0.0005842      # L_ap, m
        k_ap=45             # k_ap, W/(m*C)
        D_p=0.1             # D_p -> 100 mm, m

        # Input data
        T_ap = Tc_t[0, :]
        T_i = Tc_t[1, :]
        Tbp = Tc_t[2, :]

        # Air properties
        RHO_air = 1.21 #kg/m3
        CP_air = 1005 #J/kgC

        # Radiative heat transfer coefficients
        e_s = 0.9 # Emissivity factor
        sigma = 5.67051*1e-8 # Stefan Boltzmann constant, W/m2K4

        #Divide collector into n parts, initialization for each part 
        Area = np.zeros((self.n_collector, 1)) + \
               (H * L / self.n_collector) * np.ones((self.n_collector, 1))

        R_ap = L_ap / (k_ap * Area[0 ,0])
        m_ap = RHO_air * V_s * Area[0, 0]

        v_air = np.zeros((self.n_collector))

        for i in range(self.n_collector):
            v_air[i] = (i+1)*m_ap/(RHO_air*L*D_p)


        Tsky = np.zeros((self.n_collector, 1))
        eta = np.zeros((self.n_collector, 1))
        
        # Replacing IT with IT, variable period is degenerate

        q_sp = IT*alpha_ap*Area[0, 0]
        mu = (17.2+0.046*T_i)*(1e-6)
        k_air = 0.00007*T_i+0.0242

        #Sky temperature
        Tsky = T_o*(0.711+0.0056*T_dp+0.000073*np.power(T_dp, 2)\
            +0.013*np.cos(np.pi*(TIME-24*(doy-1))/12))                
        #Nu between plate and ambient
        Nuao = np.power((wavelength/(amplitude)),(-4.5431))\
            *np.power((amplitude/slopes),(-21.484))\
            *np.power((crest/wavelength),(0.0132))\
            *((np.power(0.7613*(RHO_air*v*H/(self.n_collector*mu)),(0.8449)))\
            +(108.17*np.power((RHO_air*V_s*H/(self.n_collector*mu)),(0.6844))))   
        # CHTC between plate and ambient
        hao=Nuao*k_air/(H/self.n_collector)
        #Thermal resistance for hao
        Rao=1./(hao*Area[0, 0])+R_ap
        hso= sigma*e_s*((np.power((T_ap+273*np.ones(np.shape(T_ap))),2)+np.power((Tsky+273*np.ones(np.shape(Tsky))),2))\
            *((T_ap+273*np.ones(np.shape(T_ap)))+(Tsky+273*np.ones(np.shape(Tsky)))))
                    
        # Nu between plate and cavity air
        Nua=np.power((wavelength/(amplitude)),(-10.66))*\
            np.power((amplitude/D_p),0.062725)*np.power((crest/wavelength),(-0.082949))*\
            np.power((amplitude/slopes),(-50.639))*(13.353*\
            np.power((RHO_air*v*H/(self.n_collector*mu)),0.85081)+3909.5*np.power((RHO_air*V_s*H/(self.n_collector*mu)),0.89008))
                
        # CHTC between plate and cavity air
        hac=Nua*k_air/(H/self.n_collector)
        # Thermal resistance for ha
        Rac=1./np.array(hac*Area[0, 0])
        # Nu between the cavity air and back wall
        Nub=1.1*0.0158*np.power((((2*D_p*RHO_air)*v_air.T)/mu), 0.8)
        # CHTC between the cavity air and back wall
        hb=Nub*k_air/D_p
        # Thermal resistanc for hb
        Rb=1./(hb*Area[0, 0])
        # CHTC between plate and sky
        hab=sigma*(np.power((T_ap+273*np.ones(np.shape(T_ap))),2)-np.power((Tbp+273*np.ones(np.shape(Tbp))),2))/(T_ap-Tbp)/(2*(1-e_s)/e_s+1)
        Rab=1./(hab*Area[0, 0])
        # Exterior effectiveness       
        epsilon_s=1./(0.38781+0.002139*np.power(wavelength/amplitude,7.3495)*np.power(amplitude/slopes,33.285)\
            *np.power(crest/wavelength,-0.074961)*np.power(np.power((RHO_air*V_s*H/(self.n_collector*mu)),2)/((RHO_air*v*H/(self.n_collector*mu))),0.13266))
        #Air temperature through perforation 
        Ts=epsilon_s*(T_ap-T_o)+T_o
        Tbp1 =(T_ap/Rab+T_i/Rb)/(1./Rab+1./Rb)
        T_ap1=(q_sp+((T_o/Rao)+(Tbp/Rab)+(T_i/Rac)+Tsky/(hso/Area[0, 0])))\
            /((1./Rao)+(1./Rab)+(1./Rac)+hso*Area[0, 0])
        T_i[0] = (m_ap*CP_air*Ts[0]+(Tbp[0]/Rb[0])\
            +(T_ap[0]/Rac[0]))/(m_ap*CP_air+(1./Rb[0]) + (1./Rac[0]))
            
        for k in range(1, self.n_collector):
            T_i[k]=(m_ap*CP_air*Ts[k]+k*m_ap*CP_air*T_i[k-1]+\
               (Tbp1[k]/Rb[k])+(T_ap1[k]/Rac[k]))/((k+1)*m_ap*CP_air+\
               (1/Rb[k]) + (1/Rac[k]))

        # Thermal heat gain, W   
        q=self.n_collector*m_ap*CP_air*(T_i[self.n_collector-1] - T_o)
            
        if IT<100.:
            eta=0
        else:
            eta=q/(IT*Area[0, 0])

           
        Tc_next=np.vstack((T_ap1, T_i, Tbp1))
        Ti=T_i[-1]
        
        return Tc_next, Ti

    def collector_dynamics(self, Tc_t, IT, T_o, v, T_dp, TIME, doy):
        # Collector dynamics updated with Tc_next
        Tc_next, Ti = self.collector_dynamics_half(Tc_t, IT, T_o, v, T_dp, TIME, doy)
        Tc_next, Ti = self.collector_dynamics_half(Tc_next, IT, T_o, v, T_dp, TIME, doy)
        return Tc_next, Ti    
    
    def system_matrices(self):
        """
        Return system matrices A and B in x_next = A*x_k + B*u_k
        """
        
        # Properties
        Roe=1/49.91
        Rer=1/811.12
        Rrf=1/703.59
        Rft=1/434.28
        Rta=1/66.65
        Ror=1/211.52
        Ref=1/425.06
        CMe=1000*1.25
        CMr=1000*1.26
        CMf=1000*19.63
        CMt=1000*24.61
        Rfa=1/205.1

        # A B X U matrix
        A=np.zeros((4,4))
        B=np.zeros((4,7))

        # A Matrix
        A[0,0]=-(1/(CMe*Roe)+1/(CMe*Rer)+1/(CMe*Ref)) 
        A[0,1]=1/(CMe*Rer)
        A[0,2]=1/(CMe*Ref)
        A[1,0]=1/(CMr*Rer)
        A[1,1]=-(1/(CMr*Rer)+1/(CMr*Rrf)+1/(CMr*Ror))
        A[1,2]=1/(CMr*Rrf)
        A[2,0]=1/(CMf*Ref)
        A[2,1]=1/(CMf*Rrf)
        A[2,2]=-(1/(CMf*Rrf)+1/(CMf*Rft)+1/(CMf*Ref)+1/(CMf*Rfa))
        A[2,3]=1/(CMf*Rft)
        A[3,2]=1/(CMt*Rft);
        A[3,3]=-(1/(CMt*Rft)+1/(CMt*Rta))
        
        A+=np.eye(4)
        
        # B Matrix
        B[0,0]=1/CMe
        B[0,4]=1/(CMe*Roe)
        B[1,1]=1/CMr
        B[1,4]=1/(CMr*Ror)
        B[2,2]=1/CMf
        B[2,6]=1/(CMf*Rfa)
        B[3,3]=1/CMt
        B[3,5]=1/(CMt*Rta)
        
        B_u = B[:, 3]
        return A, B, B_u
    
    def state_space_derivatives(self):
        A, B, B_u = self.system_matrices()
        return self.up_scale*(np.dot(A, B_u) + B_u)
    
    def State_Space_half(self, x_t, u_t, v_t):
        
        # inputs:
        # x_t is the current temperature states [Tenv Troom Tfl Ttank Tc_t]' in C.
        # Tc_t is a (3,13) vector that contains information on all the current temperature of all nodes on the collector
        # u_t is the system input, Qhp in W.
        # v_t is the known input vector [Tout(n), Qsg2, v, T_dp, Time, doy, GHI_clr, RH, I_t].
        # I_t is the global horizontal solar irradiance in W/m2.
        # Please note that the building system dynamics need to be updated every half hours, 
        # while the control decisions have to be made every hour
        
        # outputs:
        # x_next is the next temperature states [Tenv Troom Tfl Ttank]' in C.

        Tamb=21
        Tadjzone=20
        u_t = u_t[0]*self.up_scale # convert to the scale of 0-30000 W
        
        A, B, B_u = self.system_matrices()
        
        Qsg1, Qsg3, IT = self.Solar_split(v_t[4], v_t[5], v_t[8], v_t[6], v_t[0], v_t[7])
        
        Uinput = np.vstack((Qsg1, v_t[1], Qsg3, u_t, v_t[0], Tamb, Tadjzone))
        
        x_t1 = x_t[0:4]
        # print np.shape(A), np.shape(np.asarray(x_t1)), np.shape(B), np.shape(Uinput)
        x_next_1 = np.dot(A, np.reshape(x_t1, (4,1))) + np.dot(B,Uinput)
        Tc_t = x_t[4:]
        Tc_t = np.reshape(Tc_t, (3, self.n_collector))
        # This function runs for half an hour
        Tc_next, T_i = self.collector_dynamics_half(Tc_t, IT, v_t[0], v_t[2], v_t[3], v_t[4], v_t[5])
        # print np.shape(x_next_1), np.shape(Tc_next)
        x_next = np.vstack((x_next_1, np.reshape(Tc_next,(3*self.n_collector, 1))))

        return x_next    
        
    def State_Space(self, x_t, u_t, v_t):
        
        # inputs:
        # Extend original 1/2 hour function to 1 hour State_Space 
        
        x_next = self.State_Space_half(x_t, u_t, v_t)
        x_next = self.State_Space_half(x_next, u_t, v_t)
        return x_next
        
    def cost_function(self, x_t, u_t, v_t):

        # inputs:
        # x_t is the current temperature states [Tenv Troom Tfl Ttank Tc_t]' in C.
        # Tc_t is a (3,n) vector that contains information on all the current temperature of all nodes on the collector
        # u_t is the system input, Qhp in W.
        # v_t is the known input vector [Tout(n), Qsg2, v, T_dp, Time, doy, GHI_clr, RH, I_t].
        # I_t is the global horizontal solar irradiance in W/m2.
        
        # outputs:
        # Je is the electricity consumption in kW.
        
        Tc_t = x_t[4:]
        Tc_t = np.reshape(Tc_t, (3, self.n_collector))
        
        Qsg1, Qsg3, IT = self.Solar_split(v_t[4], v_t[5], v_t[8], v_t[6], v_t[0], v_t[7])
        
        Tc_next, Tbipvt=self.collector_dynamics(Tc_t, IT, v_t[0], v_t[2], v_t[3], v_t[4], v_t[5])
        
        [COP_coefficients, HC_coefficients] = self.select_heat_pump_dynamics(self.pump_type)
        
        c0=COP_coefficients[0]
        c1=COP_coefficients[1]
        c2=COP_coefficients[2]
        c3=COP_coefficients[3]
        c4=COP_coefficients[4]
        c5=COP_coefficients[5]

        COP = c0+c1*Tbipvt+c2*x_t[3]+c3*np.power(Tbipvt, 2)+c4*np.power(x_t[3], 2)+c5*x_t[3]*Tbipvt

        HC_max=self.u_t_max(x_t, v_t)
        
        u_t = u_t
        # convert to the scale of 0-30000 W
        u_t = u_t[0]*self.up_scale # Since u_t is one dimensional array element
        # print "u_t is", u_t
        if u_t <= HC_max*1000-5*1000:
            Je=u_t/(COP*1000)
        elif (u_t<=HC_max*1000) and (u_t>=HC_max*1000-5*1000):
            Je=(u_t - HC_max*1000 + 5000)/(0.9*1000) + (HC_max*1000 - 5000)/(COP*1000)
        elif u_t>1000*HC_max:
            warnings.warn('The heating power is maxed out!')
            print "Maxed out at", u_t, x_t[0:4], v_t
            Je = np.asarray(np.nan) # np.asarray(MAX_HEATING_POWER) #
        else:
            warnings.warn('The heating power is invalid!')
            # quit()
            # print "Invalidity caused by", u_t#, x_t, v_t
            Je = np.asarray(np.nan) # np.asarray(MAX_HEATING_POWER) #

        return Je
    
    def cost_function_derivative(self, x_t, u_t, v_t):
        """
        inputs:
        x_t is the current temperature states [Tenv Troom Tfl Ttank Tc_t]' in C.
        Tc_t is a (3,n) vector that contains information on all the current temperature of all nodes on the collector
        u_t is the system input, Qhp in W.
        v_t is the known input vector [Tout(n), Qsg2, v, T_dp, Time, doy, GHI_clr, RH, I_t].
        I_t is the global horizontal solar irradiance in W/m2.
        
        outputs:
        J_der: Derivative of piecewise defined cost function.
        """
        
        
        Tc_t = x_t[4:]
        Tc_t = np.reshape(Tc_t, (3, self.n_collector))
        
        Qsg1, Qsg3, IT = self.Solar_split(v_t[4], v_t[5], v_t[8], v_t[6], v_t[0], v_t[7])
        
        Tc_next, Tbipvt=self.collector_dynamics(Tc_t, IT, v_t[0], v_t[2], v_t[3], v_t[4], v_t[5])
        
        [COP_coefficients, HC_coefficients] = self.select_heat_pump_dynamics(self.pump_type)
        
        c0=COP_coefficients[0]
        c1=COP_coefficients[1]
        c2=COP_coefficients[2]
        c3=COP_coefficients[3]
        c4=COP_coefficients[4]
        c5=COP_coefficients[5]

        COP = c0+c1*Tbipvt+c2*x_t[3]+c3*np.power(Tbipvt, 2)+c4*np.power(x_t[3], 2)+c5*x_t[3]*Tbipvt

        HC_max=self.u_t_max(x_t, v_t)
        
        u_t = u_t*self.up_scale # convert to the scale of 0-30000 W
        u_t = u_t[0]# Since u_t is one dimensional array element
        # print "u_t is", u_t
        if u_t <= HC_max*1000-5*1000:
            J_der = (1./(COP*1000))*self.up_scale
        elif (u_t<=HC_max*1000) and (u_t>=HC_max*1000-5*1000):
            J_der =((1.)/(0.9*1000))*self.up_scale
        elif u_t>1000*HC_max:
            warnings.warn('The heating power is maxed out!')
            print "Maxed out at", u_t, x_t[0:4], v_t
        else:
            warnings.warn('The heating power is invalid!')

        return J_der

    def cost_function_for_plotting(self, x_t, v_t, u_t):

        # inputs:
        # x_t is the current temperature states [Tenv Troom Tfl Ttank Tc_t]' in C.
        # Tc_t is a (3,n) vector that contains information on all the current temperature of all nodes on the collector
        # u_t is the system input, Qhp in W.
        # v_t is the known input vector [Tout(n), Qsg2, v, T_dp, Time, doy, GHI_clr, RH, I_t].
        # I_t is the global horizontal solar irradiance in W/m2.
        # COP_coefficients: six-mumber list, the coefficients of the COP function
        # COP = c0+c1*Tbipvt+c2*Ttank+c3*Tbipvt^2+c4*Ttank^2+c5*Tbipvt*Ttank 

        # outputs:
        # Je is the electricity consumption in kW.
        
        Tc_t = x_t[4:]
        Tc_t = np.reshape(Tc_t, (3, self.n_collector))
        
        Qsg1, Qsg3, IT = self.Solar_split(v_t[4], v_t[5], v_t[8], v_t[6], v_t[0], v_t[7])
        
        Tc_next, Tbipvt=self.collector_dynamics(Tc_t, IT, v_t[0], v_t[2], v_t[3], v_t[4], v_t[5])

        [COP_coefficients, HC_coefficients] = self.select_heat_pump_dynamics(self.pump_type)

        c0=COP_coefficients[0]
        c1=COP_coefficients[1]
        c2=COP_coefficients[2]
        c3=COP_coefficients[3]
        c4=COP_coefficients[4]
        c5=COP_coefficients[5]

        COP = c0+c1*Tbipvt+c2*x_t[3]+c3*np.power(Tbipvt, 2)+c4*np.power(x_t[3], 2)+c5*x_t[3]*Tbipvt

        HC_max=self.u_t_max(x_t, v_t)
        
        u_t = u_t*self.up_scale # convert to the scale of 0-30000 W
        u_t = u_t[0]# Since u_t is one dimensional array element
        
        if u_t <= HC_max*1000-5*1000:
            Je=u_t/(COP*1000)
        elif (u_t<=HC_max*1000) and (u_t>=HC_max*1000-5*1000):
            Je=(u_t - HC_max*1000 + 5000)/(0.9*1000) + (HC_max*1000 - 5000)/(COP*1000)
        elif u_t>1000*HC_max:
            Je = np.asarray(np.inf)
            print "Invalidity caused by", u_t
        else:
            warnings.warn('The heating power is invalid!')
            print "Invalidity caused by", u_t

        return [Je]
    
    def u_t_max(self, x_t, v_t):

        # inputs:
        # x_t is the current temperature states [Tenv Troom Tfl Ttank Tc_t]' in C.
        # Tc_t is a (3,n) vector that contains information on all the current temperature of all nodes on the collector
        # u_t is the system input, Qhp in W.
        # v_t is the known input vector [Tout(n), Qsg2, v, T_dp, Time, doy, GHI_clr, RH, I_t].
        # I_t is the global horizontal solar irradiance in W/m2.
		# HC_coefficients: six-mumber list, the coefficients of the heat capacity function
		# HC = d0+d1*Tbipvt+d2*Ttank+d3*Tbipvt^2+d4*Ttank^2+d5*Tbipvt*Ttank
        
        # outputs:
        # HC_max is the maximum value of u_t, on the scale of 0-30 kW.

        [COP_coefficients, HC_coefficients] = self.select_heat_pump_dynamics(self.pump_type)

        c0=HC_coefficients[0]
        c1=HC_coefficients[1]
        c2=HC_coefficients[2]
        c3=HC_coefficients[3]
        c4=HC_coefficients[4]
        c5=HC_coefficients[5]

        Tc_t = x_t[4:]
        Tc_t = np.reshape(Tc_t, (3, self.n_collector))

        Qsg1, Qsg3, IT = self.Solar_split(v_t[4], v_t[5], v_t[8], v_t[6], v_t[0], v_t[7])

        Tc_next, Tbipvt = self.collector_dynamics(Tc_t, IT, v_t[0], v_t[2], v_t[3], v_t[4], v_t[5])
        HC = c0+c1*Tbipvt+c2*x_t[3]+c3*np.power(Tbipvt,2)+c4*np.power(x_t[3],2)+c5*x_t[3]*Tbipvt
        val=HC+5
        val = np.asfarray(np.array(val))

        return val
        
    def temp_min(self, x_t, u_t, v_t):
        """
        inputs:
        x_t is the current temperature states [Tenv Troom Tfl Ttank]' in C.
        u_t is the system input, Qhp in W.
        v_t is the known input vector [Tout(n), Qsg2, v, T_dp, Time, doy, GHI_clr, RH, I_t].
        I_t is the global horizontal solar irradiance in W/m2.
        
        outputs:
        f2_val is equivalent to (Tmin-x_next), this value should be no more than 0.
        """
        
        x_next=self.State_Space(x_t, u_t, v_t)
        
        if (v_t[5]+0.5>=0) and (v_t[5]+0.5<8):
            Tmin= np.reshape([-np.Inf, 15, 19, 25], (4, 1))
        elif (v_t[5]+0.5>=8) and (v_t[5]+0.5<10.5):
            Tmin= np.reshape([-np.Inf, 17, 19, 25], (4, 1))
        elif (v_t[5]+0.5>=10.5) and (v_t[5]+0.5<19):
            Tmin= np.reshape([-np.Inf, 21, 19, 25], (4, 1))
        else:
            Tmin= np.reshape([-np.Inf, 15, 19, 25], (4, 1))
        
        f2_val=Tmin-x_next[0:4]
        
        return f2_val
        
    def temp_max(self, x_t, u_t, v_t):
        """
        inputs:
        x_t is the current temperature states [Tenv Troom Tfl Ttank]' in C.
        u_t is the system input, Qhp in W.
        v_t is the known input vector [Tout(n), Qsg2, v, T_dp, Time, doy, GHI_clr, RH, I_t].
        I_t is the global horizontal solar irradiance in W/m2.
        
        outputs:
        f1_val is equivalent to (x_next-T_max), this value should be no more than 0.
        """

        x_next=self.State_Space(x_t, u_t, v_t)
        
        Tmax= np.reshape([np.Inf, 25, 29, 55], (4, 1))
        
        f1_val=x_next[0:4]-Tmax
        
        return f1_val
        
    def constraints(self, x_t, u_t, v_t):

        """
        inputs:
        x_t is the current temperature states [Tenv Troom Tfl Ttank]' in C.
        u_t is the system input, Qhp in W.
        v_t is the known input vector [Tout(n), Qsg2, v, T_dp, Time, doy, GHI_clr, RH, I_t].
        I_t is the global horizontal solar irradiance in W/m2.
        
        outputs:
        f1_val is equivalent to (x_next-T_max), this value should be no more than 0.
        f2_val is equivalent to (Tmin-x_next), this value should be no more than 0.
        f3_val is equivalent to (u_t-HC_max), this value should be no more than 0.
        """
        u_t = u_t*self.up_scale # convert to the scale of 0-30000 W
        HC_max=self.u_t_max(x_t, v_t)
        f3_val=(u_t-HC_max*1e3)*self.down_scale
        
        u_t = u_t*self.down_scale
        f2_val= self.temp_min(x_t, u_t, v_t)
        f1_val= self.temp_max(x_t, u_t, v_t)
        
        return f1_val, f2_val, f3_val

    def constraint_function_values_derivative(self):
        """
        inputs:
        
        outputs:
        f1_val_der is derivative of (x_next-T_max).
        f2_val_der is derivative of (Tmin-x_next).
        f3_val_der is derivative of (u_t-HC_max).
        """
        state_space_derivatives = self.state_space_derivatives()
        
        f3_val_der = [1.]
        
        # Better change the constraints function to return only 3 values
        # This requires changes here and in a function in 'stochastic_dynamics_building_control.py'
        # Remove dummy constraints, Consider only the last 3, as first constraint is not included (it's Inf)
        f2_val_der = -1.*state_space_derivatives[1:]
        f1_val_der = state_space_derivatives[1:]
        
        constraint_derivatives = np.hstack((f1_val_der, f2_val_der))
        constraint_derivatives = np.hstack((constraint_derivatives, f3_val_der)).tolist()
        # This step is needed for SLSQP; Matches with constraint function in stochastic_dynamics_building_control
        constraint_derivatives = [-1*i for i in constraint_derivatives]
        return constraint_derivatives
        
        
    def select_heat_pump_dynamics(self, pump_type):

        """
        inputs:
        pump_type: 1, Maroon2 MT29; 2, DAIKIN ALTHERMA ERLQ048BAVJU

        outputs:
        COP_coefficients: six-mumber list, the coefficients of the COP function
            COP = c0+c1*Tbipvt+c2*Ttank+c3*Tbipvt^2+c4*Ttank^2+c5*Tbipvt*Ttank 
        HC_coefficients: six-mumber list, the coefficients of the heat capacity function
            HC = d0+d1*Tbipvt+d2*Ttank+d3*Tbipvt^2+d4*Ttank^2+d5*Tbipvt*Ttank
        """

        if pump_type == 1:
            COP_coefficients = [6.2504, 0.1338, -0.0986, 0.005864, 0.000415, -0.0015]
            HC_coefficients = [25.3537, 0.7337, -0.0623, 0.0056, 0.000104, -0.0041]
        elif pump_type == 2:
            COP_coefficients = [7.8885, 0.1809, -0.1568, 0.001068, 0.0009938, -0.002674]
            HC_coefficients = [13.301, 0.4035, -0.02868, 0.003455, -0.0003845, -0.002958]

        return COP_coefficients, HC_coefficients


if __name__ == '__main__':
    print 'RUNNING'
    dyn_prog = DynamicProgrammingProblem()

    n =13
    Tc_t=np.random.rand(3,13)+5
    x_t=np.zeros((43,1))
    x_t[0]=20
    x_t[1]=20
    x_t[2]=22
    x_t[3]=30
    x_t[4:]=np.reshape(Tc_t, (3*n, 1))
    u_t=30*1000
    v_t=np.array([5, 1800, 2, 2, 12, 100, 500, 50, 400])
    x_next = dyn_prog.State_Space_half(x_t, u_t, v_t)
    x_next_1 = dyn_prog.State_Space(x_t, u_t, v_t)
    u_max = dyn_prog.u_t_max(x_t, v_t)
    
    print dyn_prog.state_space_derivatives()
    print dyn_prog.constraint_function_values_derivative()
    print u_max
