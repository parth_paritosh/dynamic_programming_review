# dynamic_programming_review

Author: Parth Paritosh, Ilias Bilionis

As an addendum to "Model Predictive Control under Forecast Uncertainty for Optimal Operation of Buildings with Integrated Solar Systems"
Co-authored by Xiaoqi Liu, Parth Paritosh, Nimish Awalgaonkar, Ilias Bilionis, Panagiota Karava.

Written as Python2.7 code and Shell script

Standard Python libraries: numpy, math, matplotlib, scipy, pandas, os, sys, time, cPickle, itertools, warnings, copy, functools, collections, csv 
Other Python libraries (may not be included in standard installation): mpi4py, GPy

The code is separated in sections of source and visualization folders. The code can be implemented by running the following commands in succession.

Generate dynamic programming inputs by running 'python simulation_input_data.py' with date and hour set in the file

Run algorithm implementation as 'mpiexec -n 8 python bldg_ctrl_mat_mpi.py >> log_mpi.txt 2>> err_mpi.txt'

The number 8 specifies number of cores used in parallel computation

Run visualization as 'python visualization_bldg_ctrl_DP.py' after selecting the functions in the file

Note: Can change optimization library at line 18 (pyOpt) and 19 (scipy.minimize)

File descriptions:

bldg_ctrl_mat_mpi.py: Contains top loop of value iteration algorithm. 

pyOpt_step_value_iteration_building_control.py: Implements optimization at a point in discrete grid at a time step using pyOpt library

step_value_iteration_building_control.py: Implements optimization at a point in discrete grid at a time step using scipy minimize library

stochastic_dynamics_building_control.py: Implements the single step stochastic dynamics and computes stage cost for given point in state space

building_control.py: Implements single step dynamics of the building.

load_uncertain.py: Include weather and irradiation data

_mpi.py: A wrapper on mpi library

func_approximator.py: Implements a wrapper on Gaussian process library (https://github.com/SheffieldML/GPy) to make it multi input(created by https://github.com/PredictiveScienceLab) 

visualization_bldg_ctrl_DP.py: Plots deterministic and stochastic (irradiation samples) inputs, value functions, simulated building temperature, optimal policies and animation. Can be used from main section of the file.

Library structure of code as discussed at https://stackoverflow.com/questions/1260792/import-a-file-from-a-subdirectory